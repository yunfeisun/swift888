//
//  YFAPI.swift
//  robin8Swift
//
//  Created by RB8 on 2017/12/18.
//  Copyright © 2017年 robin8. All rights reserved.
//

import Foundation
import Moya
import MBProgressHUD
import HandyJSON
import SwiftyJSON
import AdSupport
let LoadingPlugin = NetworkActivityPlugin{(type,target) in
    guard let vc = topVC else{return}
    switch type{
    case .began:
        MBProgressHUD.hide(for: vc.view, animated: false)
        MBProgressHUD.showAdded(to: vc.view, animated: true)
    case .ended:
        MBProgressHUD.hide(for: vc.view, animated: true)
    }
}
let timeoutClosure = {(endpoint:Endpoint<YFAPI>,closure:MoyaProvider<YFAPI>.RequestResultClosure) -> Void in
    if var urlRequest = try? endpoint.urlRequest() {
        urlRequest.timeoutInterval = 20
        closure(.success(urlRequest))
    }else{
        closure(.failure(MoyaError.requestMapping(endpoint.url)))
    }
}
let myEndpointClosure = {(target:YFAPI) -> Endpoint<YFAPI> in
    let url = target.baseURL.appendingPathComponent(target.path).absoluteString
    
    let endpoint: Endpoint<YFAPI> = Endpoint<YFAPI>.init(url: url, sampleResponseClosure: {.networkResponse(200, target.sampleData)}, method: .post, task: target.task, httpHeaderFields: ["Authorization":JWTSecret.getSecret()])
//    (
//        url:url,
//        sampleResponseClosure:{.networkResponse(200, target.sampleData)},
//        method:target.method,
//        task:target.task, httpHeaderFields: ["Authorization":JWTSecret.getSecret()]
//
////        httpHeaderFields: target.headers
//    )
    print(url)
    print(target.method)
    print(target.task)
    return endpoint
}

let YFHeaderProvider = MoyaProvider<YFAPI>(endpointClosure:myEndpointClosure)

let YFProvider = MoyaProvider<YFAPI>(requestClosure:timeoutClosure)
let YFLoadingProvider = MoyaProvider<YFAPI>()
enum YFAPI {
    case HomeUrl(page:Int,banner:String,order:String)//首页
    case CampainUrl(page:Int,banner:String,order:String)//活动页
    case CampainDetailUrl(id:String)//活动页
    case Login(phone:String,code:String) //登录
    case GetCode(phone:String)//获取验证码
    case GetRBUserInfo//我的界面
    case GetCampainData(status:String,title:String,message:String,page:String)//活动页面
    case GetDetailCampainData(invitee_page:String,activityID:String)
}
extension YFAPI:TargetType{

    var method: Moya.Method {
        switch self {
        case .GetCode(phone:),.Login:
            return .post
        default:
            return .get
        }
    }
    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
   
    var task:Task{
        var parmeters = [String:String]()
        
        switch self {
        case .HomeUrl(let page,let banner,let order):
            parmeters["page"] = String(page)
            parmeters["order"] = order
            parmeters["with_kol_announcement"] = banner
        case .CampainUrl(let page,let banner, let order):
            parmeters["page"] = String(page)
            parmeters["order"] = order
            parmeters["with_kol_announcement"] = banner
        case .CampainDetailUrl(let kolId):
            parmeters["id"] = kolId
        case .GetCode(var phone):
            phone = phone.replacingOccurrences(of: " ", with: "")
            phone = phone.trimmingCharacters(in: CharacterSet.whitespaces)
            parmeters["mobile_number"] = phone
        case .Login(var telPhone,let code):
            telPhone = telPhone.replacingOccurrences(of: " ", with: "")
            telPhone = telPhone.trimmingCharacters(in: CharacterSet.whitespaces)
            parmeters["mobile_number"] = telPhone
            parmeters["code"] = code
            parmeters["app_platform"] = "IOS"
            parmeters["utm_source"] = "AppStore"
            parmeters["os_version"] = PublicAPI.getCurrentDeviceSystemVersion()
            parmeters["app_version"] = PublicAPI.getCurrentBundleShortVersion()
            parmeters["device_model"] = "iphone7"
            parmeters["city_name"] = "上海"
            parmeters["device_token"] = "abdiekx,xfp"
            let a  = ASIdentifierManager.shared().advertisingIdentifier.uuidString
            parmeters["IDFA"] = a
            parmeters["kol_uuid"] = nil
        case .GetRBUserInfo:
            break
        case .GetCampainData(let status,let title,let message,let page):
            parmeters["status"] = status
            parmeters["title"] = title
            parmeters["message"] = message
            parmeters["page"] = page
        case .GetDetailCampainData(_,_):
            parmeters["invitee_page"] = "1"
        }
         return .requestParameters(parameters: parmeters, encoding: URLEncoding.default)
        
    }
    var parameterEncoding:ParameterEncoding{
        return URLEncoding.default
    }
    
    var headers: [String : String]? {
        switch self {
        case .CampainDetailUrl,.GetRBUserInfo,.GetCampainData,.GetDetailCampainData:
            return ["Authorization":JWTSecret.getSecret()]
        case .GetCode:
            return ["Authorization":JWTSecret.getSecret()]
        default:
            return nil
        }
    }
    var baseURL:URL {return URL(string:APIURL)!}
    var path: String{
        switch self{
        case .HomeUrl:return "api/v1_6/big_v"
        case .CampainUrl:return "/api/v1_6/big_v"
        case .CampainDetailUrl(let kolId):
            let str = "api/v1_6/big_v/\(kolId)/detail"
            return str
        case .GetCode:return "api/v1/phones/get_code"
        case .Login: return "api/v2/kols/sign_in"
        case .GetRBUserInfo: return "api/v1_6/my/show"
        case .GetCampainData: return "api/v1/campaign_invites"
        case .GetDetailCampainData( _,let activityID):
            return "api/v1/campaigns/" + "\(activityID)"
        }
    }
}

extension MoyaProvider{
    func request1(_ target:Target,completion:((_ returnData:JSON) -> Void)?,failure:@escaping ((_ error:Error) -> Void)) {
        
        request(target) { (result) in
            switch result{ 
            case let .success(response):
                
                let data = try? response.mapJSON()
                let json = JSON(data!)
                guard let completion2 = completion else{return}
                completion2(json)
            case let .failure(error):
                failure(error)
            }
        }
    }
}


















