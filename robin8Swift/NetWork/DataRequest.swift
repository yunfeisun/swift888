//
//  DataRequest.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/15.
//  Copyright © 2017年 robin8. All rights reserved.
//

import Foundation
class RequestData{
    //kol界面
    class   func KOLRequest(page:String, with_kol_announcement:String?, order:String,success:@escaping ([String:Any]?)->(),failture: @escaping (NSError)->()) {
        var dic = [String:String]()
        dic["page"] = page
        dic["order"] = order
        if with_kol_announcement != nil {
            dic["with_kol_announcement"] = with_kol_announcement

        }
        NetworkTools.GETJSON(URLString:"http://robin8.net/api/v1_6/big_v", params: dic,headers:nil, success: { (result) in
            //            //JSON的String转model
            //            if let testModel = TestModel.deserialize(from:result)
            //            {//model处理后传出
            //                print(testModel);
            //                success(testModel)
            //            }
            success(result)
        }) { (error) in
            // 传出错误信息
            failture(error)
        }
    }
   class func KOLDetail(kolId:String,success:@escaping ([String:Any]?)->(),failture:@escaping(NSError)->()) {
        var dic = [String:String]()
        dic["id"] = kolId
        var head = [String:String]()
        head["Authorization"] = JWTSecret.getSecret()
        NetworkTools.GETJSON(URLString: APIURL+"/api/v1_6/big_v/"+kolId+"/detail", params: dic,headers:head, success: { (result) in
            success(result)
        }) { (error) in
            failture(error)
        }
    }
    class func sendVerificationcode(phone:String,success:@escaping ([String:Any]?)->(),failture:@escaping(NSError)->()){
        var dic = ["mobile_number" : phone]
//    dic["mobile_number"] = phone
//    var head = [String:String]()
//    head["Authorization"] = JWTSecret.getSecret()
    print(JWTSecret.getSecret())
    print(phone)
 //   print(head)
        NetworkTools.POSTJSON(URLString: APIURL+"api/v1/phones/get_code", params: dic, headers: nil, success: { (result) in
            success(result)
        }) { (error) in
            failture(error)
        }
//    NetworkTools.GETJSON(URLString: APIURL+"api/v1/phones/get_code", params: dic,headers:nil, success: { (result) in
//    success(result)
//    }) { (error) in
//    failture(error)
//    }
    }
}
