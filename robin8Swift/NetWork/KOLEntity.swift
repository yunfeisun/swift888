//
//  KOLEntity.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/15.
//  Copyright © 2017年 robin8. All rights reserved.
//

import Foundation
import HandyJSON
import SwiftyJSON
//KOL
struct KOLModel:HandyJSON {
    var avatar_url = ""
    var id = ""
    var job_info = ""
    var name = ""
    var tags = [tagModel]()
    
}
struct tagModel:HandyJSON {
    var label = ""
    var name = ""
}
struct bannerModel:HandyJSON {
    var category:String?
    var content:String?
    var cover_url:String?
    var title:String?
}
struct kolEntity {
    var avatar_url:String?
    var id:String?
    var job_info:String?
    var name:String?
    var tags:[Any]?
    
    init(jsonData:JSON) {
        avatar_url = jsonData["avatar_url"].stringValue
        id = jsonData["id"].stringValue
        job_info = jsonData["job_info"].stringValue
        name = jsonData["name"].stringValue
        tags = jsonData["tags"].arrayObject
    }
}

struct kolBannerModel {
    var category:String?
    var content:String?
    var cover_url:String?
    var title:String?
    init(jsonData:JSON) {
        category = jsonData["category"].stringValue
        content = jsonData["content"].stringValue
        cover_url = jsonData["cover_url"].stringValue
        title = jsonData["title"].stringValue
    }
}
struct kolDetailModel {
    var avatar_url:String?
    var desc:String?
    var name:String?
    init(jsonData:JSON) {
        avatar_url = jsonData["avatar_url"].stringValue
        desc = jsonData["desc"].stringValue
        name = jsonData["name"].stringValue
    }
}
struct tagIdentity {
    var label:String?
    var name:String?
    init(jsonData:JSON) {
        label = jsonData["label"].stringValue
        name = jsonData["name"].stringValue
    }
}
struct myEntity {
    var id:String?
    var kol_role:String?
    var kol_uuid:String?
    var avatar_url:String?
    var name:String?
    var influence_score:String?
    var issue_token:String?
    var app_city_label:String?
    var rongcloud_token:String?
    var mobile_number:String?
    var age:String?
    var alipay_account:String?
    var app_city:String?
    var selected_like_articles:String?
    var alipay_name:String?
    var email:String?
    var gender:String?
    var desc:String?
    var role_apply_status:String?
    var id_card:String?
    var date_of_birthday:String?
    var role_check_remark:String?
    var tags:[tagIdentity] = []
    
    init(jsonData:JSON) {
       id = jsonData["id"].stringValue
       kol_role = jsonData["kol_role"].stringValue
       kol_uuid = jsonData["kol_uuid"].stringValue
       avatar_url = jsonData["avatar_url"].stringValue
       name = jsonData["name"].stringValue
       influence_score = jsonData["influence_score"].stringValue
       issue_token = jsonData["issue_token"].stringValue
       app_city_label = jsonData["app_city_label"].stringValue
       rongcloud_token = jsonData["rongcloud_token"].stringValue
       mobile_number = jsonData["mobile_number"].stringValue
       age = jsonData["age"].stringValue
       alipay_account = jsonData["alipay_account"].stringValue
       app_city = jsonData["app_city"].stringValue
       selected_like_articles = jsonData["selected_like_articles"].stringValue
       selected_like_articles = jsonData["selected_like_articles"].stringValue
       alipay_name = jsonData["alipay_name"].stringValue
       email = jsonData["email"].stringValue
       gender = jsonData["gender"].stringValue
       desc = jsonData["desc"].stringValue
       role_apply_status = jsonData["role_apply_status"].stringValue
       id_card = jsonData["id_card"].stringValue
       date_of_birthday = jsonData["date_of_birthday"].stringValue
       role_check_remark = jsonData["role_check_remark"].stringValue
       for index in 1...jsonData["tags"].count {
        let entity = tagIdentity.init(jsonData: jsonData["tags"][index - 1])
        tags.append(entity)
       }
    }
}
struct personalKolEntity {
    var kol_role:String?
    var name:String?
    var avg_campaign_credit:String?
    var id:String?
    var max_campaign_earn_money:String?
    var avatar_url:String?
    init(jsonData:JSON) {
        kol_role = jsonData["kol_role"].stringValue
        name = jsonData["name"].stringValue
        avg_campaign_credit = jsonData["avg_campaign_credit"].stringValue
        id = jsonData["id"].stringValue
        max_campaign_earn_money = jsonData["max_campaign_earn_money"].stringValue
        avatar_url = jsonData["avatar_url"].stringValue
    }
}
struct PersonalEntity {
    var hide:String?
    var error:String?
    var detail:String?
    var kol:personalKolEntity?
    init(jsonData:JSON) {
        hide = jsonData["hide"].stringValue
        error = jsonData["error"].stringValue
        detail = jsonData["detail"].stringValue
        kol = personalKolEntity.init(jsonData: jsonData["kol"])
    }
}
//活动列表
struct campainEntity {
    var img_status:String?
    var tag:String?
    var invite_status:String?
    var id:String?
    var earn_money:String?
    var status:String?
    var sale_price:String?
    var approved_at:String?
    var campaign:subCampainEntity?
    init(jsonData:JSON) {
        img_status = jsonData["img_status"].stringValue
        tag = jsonData["tag"].stringValue
        invite_status = jsonData["invite_status"].stringValue
        id = jsonData["id"].stringValue
        earn_money = jsonData["earn_money"].stringValue
        status = jsonData["status"].stringValue
        sale_price = jsonData["sale_price"].stringValue
        approved_at = jsonData["approved_at"].stringValue
        campaign = subCampainEntity.init(jsonData:jsonData["campaign"])
    }
}
//
struct subCampainEntity {
    var budget:String?
    var remain_budget:String?
    var brand_name:String?
    var id:String?
    var per_action_budget:String?
    var per_action_type:String?
    var take_budget:String?
    var img_url:String?
    var total_click:String?
    var hide_brand_name:String?
    var cpi_example_screenshot:String?
    var name:String?
    var url:String?
    var start_time:String?
    var deadline:String?
    var per_budget_type:String?
    var description:String?
    var status:String?
    var recruit_end_time:String?
    var remark:String?
    var max_action:String?
    init(jsonData:JSON) {
        budget = jsonData["budget"].stringValue
        remain_budget = jsonData["remain_budget"].stringValue
        brand_name = jsonData["brand_name"].stringValue
        id = jsonData["id"].stringValue
        per_action_budget = jsonData["per_action_budget"].stringValue
        per_action_type = jsonData["per_action_type"].stringValue
        take_budget = jsonData["take_budget"].stringValue
        img_url = jsonData["img_url"].stringValue
        total_click = jsonData["total_click"].stringValue
        hide_brand_name = jsonData["hide_brand_name"].stringValue
        name = jsonData["name"].stringValue
        cpi_example_screenshot = jsonData["cpi_example_screenshot"].stringValue
        url = jsonData["url"].stringValue
        start_time = jsonData["start_time"].stringValue
        deadline = jsonData["deadline"].stringValue
        per_budget_type = jsonData["per_budget_type"].stringValue
        description = jsonData["description"].stringValue
        status = jsonData["status"].stringValue
        recruit_end_time = jsonData["recruit_end_time"].stringValue
        remark = jsonData["remark"].stringValue
        max_action = jsonData["max_action"].stringValue
    }
}
struct campainDetailEntity {
    var invitees_count:String?
    var img_status:String?
    var tag:String?
    var sub_type:String?
    var uuid:String?
    var is_invited:String?
    var cpi_example_screenshots:[String]?
    var invitees_cis_invitedount:String?
    var ocr_detail:String?
    var ocr_status:String?
    var invite_status:String?
    var id:String?
    var total_click:String?
    var cpi_example_screenshot:String?
    var screenshot:String?
    var avail_click:String?
    var earn_money:String?
    var can_upload_screenshot:Bool?
    var share_url:String?
    var reject_reason:String?
    var price:String?
    var status:String?
    var start_upload_screenshot:String?
    var sale_price:String?
    var approved_at:String?
    var screenshots:[String]?
    var screenshot_comment:[String]?
    var campaign:subDetailCampainDetailEntity?
    var invitees:[detailUserEntity] = []
    var upload_interval_time : [String]?
    
    init(jsonData:JSON) {
        let jsonValue = jsonData["campaign_invite"]
        invitees_count = jsonData["budget"].stringValue
        img_status = jsonValue["img_status"].stringValue
        tag = jsonValue["tag"].stringValue
        sub_type = jsonValue["sub_type"].stringValue
        uuid = jsonValue["uuid"].stringValue
        is_invited = jsonValue["is_invited"].stringValue
        ocr_detail = jsonValue["ocr_detail"].stringValue
        ocr_status = jsonValue["ocr_status"].stringValue
        invite_status = jsonValue["invite_status"].stringValue
        id = jsonValue["id"].stringValue
        cpi_example_screenshot = jsonValue["cpi_example_screenshot"].stringValue
        screenshot = jsonValue["screenshot"].stringValue
        avail_click = jsonValue["avail_click"].stringValue
        earn_money = jsonValue["earn_money"].stringValue
        can_upload_screenshot = jsonValue["can_upload_screenshot"].boolValue
        reject_reason = jsonValue["reject_reason"].stringValue
        price = jsonValue["price"].stringValue
        status = jsonValue["status"].stringValue
        start_upload_screenshot = jsonValue["start_upload_screenshot"].stringValue
        approved_at = jsonValue["approved_at"].stringValue
        cpi_example_screenshots = jsonValue["cpi_example_screenshots"].arrayObject as? [String]
        screenshots = jsonValue["screenshots"].arrayObject as? [String]
        screenshot_comment = jsonValue["screenshot_comment"].arrayObject as? [String]
        campaign = subDetailCampainDetailEntity.init(jsonData: jsonValue["campaign"])
        upload_interval_time = jsonValue["upload_interval_time"].arrayObject as? [String]
        let arr = jsonData["invitees"]
        if arr.count > 0 {
            for index in 1...arr.count {
                let model = detailUserEntity.init(jsonData: arr[index])
                invitees.append(model)
            }
        }
    }
}
struct subDetailCampainDetailEntity {
    var budget:String?
    var remain_budget:String?
    var wechat_auth_type:String?
    var cpi_example_screenshots:[String] = []
    var brand_name:String?
    var per_action_budget:String?
    var id:String?
    var per_action_type:String?
    var take_budget:String?
    var img_url:String?
    var total_click:String?
    var hide_brand_name:String?
    var cpi_example_screenshot:String?
    var message:String?
    var share_times:String?
    var avail_click:String?
    var url:String?
    var name:String?
    var start_time:String?
    var deadline:String?
    var address:String?
    var per_budget_type:String?
    var task_description:String?
    var description:String?
    var status:String?
    var recruit_end_time:String?
    var max_action:String?
    var recruit_start_time:String?
    var remark:String?
    var action_desc:String?
    init(jsonData:JSON) {
        budget = jsonData["budget"].stringValue
        remain_budget = jsonData["remain_budget"].stringValue
        wechat_auth_type = jsonData["wechat_auth_type"].stringValue
        brand_name = jsonData["brand_name"].stringValue
        per_action_budget = jsonData["per_action_budget"].stringValue
        id = jsonData["id"].stringValue
        per_action_type = jsonData["per_action_type"].stringValue
        take_budget = jsonData["take_budget"].stringValue
        img_url = jsonData["img_url"].stringValue
        total_click = jsonData["total_click"].stringValue
        hide_brand_name = jsonData["hide_brand_name"].stringValue
        cpi_example_screenshot = jsonData["cpi_example_screenshot"].stringValue
        message = jsonData["message"].stringValue
        share_times = jsonData["share_times"].stringValue
        avail_click = jsonData["avail_click"].stringValue
        url = jsonData["url"].stringValue
        name = jsonData["name"].stringValue
        start_time = jsonData["start_time"].stringValue
        deadline = jsonData["deadline"].stringValue
        address = jsonData["address"].stringValue
        per_budget_type = jsonData["per_budget_type"].stringValue
        task_description = jsonData["task_description"].stringValue
        description = jsonData["description"].stringValue
        status = jsonData["status"].stringValue
        recruit_end_time = jsonData["recruit_end_time"].stringValue
        max_action = jsonData["max_action"].stringValue
        recruit_start_time = jsonData["recruit_start_time"].stringValue
        remark = jsonData["remark"].stringValue
        action_desc = jsonData["action_desc"].stringValue
        cpi_example_screenshots = (jsonData["cpi_example_screenshots"].arrayObject as? [String])!
    }
}
struct detailUserEntity {
    var id:String?
    var name:String?
    var avatar_url:String?
    init(jsonData:JSON) {
        id = jsonData["id"].stringValue
        name = jsonData["name"].stringValue
        avatar_url = jsonData["avatar_url"].stringValue
    }
}
class dealData {
    //KOL界面
    class  func kolData(jsonObject:JSON) -> NSArray {
        var arr:[kolEntity] = []
         let arr1 = jsonObject["big_vs"].arrayValue
            for index in 1...arr1.count{
                let jsonData = JSON(arr1[index - 1])
                let kolModel = kolEntity.init(jsonData: jsonData)
                arr.append(kolModel)
            }
        return arr as NSArray
    }
    //KOL详情
    class func kolDetailData(jsonObject:JSON) -> kolDetailModel? {
        if jsonObject["big_v"] != JSON.null {
            let jsonData = JSON(jsonObject["big_v"])
            let model = kolDetailModel.init(jsonData:jsonData)
            return model
        }
        return nil
    }
    class func loginData(jsonObject:JSON) -> myEntity? {
        if jsonObject != JSON.null {
            let jsonData = JSON(jsonObject)
            let model = myEntity.init(jsonData: jsonData)
            return model
        }
        return nil
    }
   class func personData(jsonObject:JSON) -> PersonalEntity? {
        if jsonObject != JSON.null {
            let jsonData = JSON(jsonObject)
            let model = PersonalEntity.init(jsonData: jsonData)
            return model
        }
        return nil
    }
    //活动列表
    class func campainData(jsonObject:JSON,backArray:[campainEntity]) -> Array<Any>? {
       
        if jsonObject != JSON.null {
            var dataArray = backArray
            let arr = jsonObject["campaign_invites"]
            if arr.count > 0{
            for index in 1...arr.count{
                let jsonData = JSON(arr[index - 1])
                let entity = campainEntity.init(jsonData: jsonData)
                dataArray.append(entity)
            }
            }
            return dataArray
        }
        return nil
    }
    //活动详情列表
   class func campaignDetail(jsonObject:JSON) -> campainDetailEntity? {
        if jsonObject != JSON.null {
            let model = campainDetailEntity.init(jsonData: jsonObject)
            return model
        }
        return nil
    }
}
//extension Array:HandyJSON{}
//struct ReturnData<T:HandyJSON>:HandyJSON {
//
//}
//struct ResponseData<T:HandyJSON>:HandyJSON {
//
//}














