//
//  DataManager.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/14.
//  Copyright © 2017年 robin8. All rights reserved.
//

import Foundation

//kol界面
func KOLRequest(page:String, with_kol_announcement:String, order:String,success:@escaping (Any?)->(),failture: @escaping (NSError)->()) {
    var dic = [String:String]()
    dic["page"] = "1"
    dic["with_kol_announcement"] = "Y"
    dic["order"] = "order_by_created"
    NetworkTools.POST(URLString:"http://robin8.net/api/v1_6/big_v", params: dic,headers:nil, success: { (result) in
//            //JSON的String转model
//            if let testModel = TestModel.deserialize(from:result)
//            {//model处理后传出
//                print(testModel);
//                success(testModel)
//            }
            success(result)
        }) { (error) in
            // 传出错误信息
            failture(error)
        }
}
