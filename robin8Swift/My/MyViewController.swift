//
//  MyViewController.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/10.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit
import Kingfisher
class MyViewController: BaseViewController,BaseVCDelegate {
   
    
   // let tableView:UITableView
    var btn:UIButton?
    var myEntity:PersonalEntity?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        YFLoadingProvider.request1(.GetRBUserInfo, completion: { (result) in
            if result["error"] == 0{
                self.myEntity = dealData.personData(jsonObject: result)
                let headImage:UIImageView = self.view.viewWithTag(100) as! UIImageView
                let url = URL.init(string: (self.myEntity?.kol?.avatar_url)!)
                headImage.kf.setImage(with: url)
                if LocalService.getRBLocalDataUserPrivateToken() != nil{
                    headImage.isHidden = false
                    self.btn?.isHidden = true
                }else{
                    headImage.isHidden = true
                    self.btn?.isHidden = false
                }
            }else{
                let headImage:UIImageView = self.view.viewWithTag(100) as! UIImageView
                headImage.isHidden = true
                self.btn?.isHidden = false
            }
        }) { (error) in
            print(error)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = true
        self.navTitle = "我的"
        self.navRightTitle = "set"
        self.navLeftTitle = nil
        self.delegate = self
        self.edgePGR?.isEnabled  = false
        btn = UIButton.init(frame: CGRect(x: (KScreenWidth - 100)/2, y: 200, width: 100, height: 50))
        btn?.setTitle("登录", for: .normal)
        btn?.setTitleColor(UIColor.red, for: .normal)
        btn?.backgroundColor = UIColor.green
        btn?.addTarget(self, action: #selector(presentLogin), for: .touchUpInside)
        btn?.isHidden = true
        self.view.addSubview(btn!)
        //
        let headImageView = UIImageView.init(frame: CGRect(x: (KScreenWidth - 75)/2, y: (KScreenHeight - 75)/2, width: 75, height: 75))
        headImageView.isUserInteractionEnabled = true
        headImageView.isHidden = true
        headImageView.tag = 100
        headImageView.layer.cornerRadius = 75/2
        headImageView.layer.masksToBounds = true
        self.view.addSubview(headImageView)
        //
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(setHeadImage))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        headImageView.addGestureRecognizer(tap)
    }
    @objc func setHeadImage() {
        if LocalService.getRBLocalDataUserPrivateToken() == nil {
            self.presentLogin()
        }else{
            let masterVC = HsuAlbumMasterTableViewController()
            let navi = UINavigationController(rootViewController: masterVC)
            masterVC.title = "图片"
            let gridVC = HsuAssetGridViewController()
            gridVC.title = "所有图片"
            navi.pushViewController(gridVC, animated: false)
            present(navi, animated: true)
            
            HandleSelectionPhotosManager.share.getSelectedPhotos(with: 1) { (assets, images) in
                let headImage:UIImageView = self.view.viewWithTag(100) as! UIImageView
                headImage.image = images[0]
            }
        }
    }
    func NavRightBtnAction() {
        if LocalService.getRBLocalDataUserPrivateToken() == nil{
            self.presentLogin()
        }else{
//            let setVC = SetViewController()
//            setVC.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(setVC, animated: true)
            let toview = CropViewController()
            toview.hidesBottomBarWhenPushed = true
            
            self.navigationController?.pushViewController(toview, animated: true)
        }
    }
    @objc func presentLogin() {
        let loginVC = LoginViewController.init(nibName: "LoginViewController", bundle: nil)
        self.present(loginVC, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
