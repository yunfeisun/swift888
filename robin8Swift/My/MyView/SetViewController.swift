//
//  SetViewController.swift
//  robin8Swift
//
//  Created by RB8 on 2018/1/19.
//  Copyright © 2018年 robin8. All rights reserved.
//

import UIKit

class SetViewController: BaseViewController,BaseVCDelegate  {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navTitle = "设置"
        self.delegate = self
        //
        let loginOutBtn = UIButton.init(frame: CGRect(x: 0, y: 64, width: KScreenWidth, height: 50))
        loginOutBtn.setTitle("退出登录", for: .normal)
        loginOutBtn.setTitleColor(UIColor.black, for: .normal)
        loginOutBtn.layer.borderColor = UIColor.brown.cgColor
        loginOutBtn.layer.masksToBounds = true
        loginOutBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        loginOutBtn.backgroundColor = UIColor.white
        loginOutBtn.addTarget(self, action: #selector(loginOut(sender:)), for:.touchUpInside)
        self.view.addSubview(loginOutBtn)
        // Do any additional setup after loading the view.
    }
    @objc func loginOut(sender:UIButton){
        LocalService.setRBLocalDataUserLoginIdWith(str: nil)
        LocalService.setRBLocalDataUserLoginNameWith(str: nil)
        LocalService.setRBLocalDataUserLoginPhoneWith(str: nil)
        LocalService.setRBLocalDataUserLoginAvatarWith(str: nil)
//        LocalService.setRBLocalDataUserPrivateTokenWith(str: nil)
        
        LocalService.setRBLocalDataUserLoginKolIdWith(str: nil)
        let ud = UserDefaults.standard
        ud.removeObject(forKey: "PrivateToken")
        self.navigationController?.popViewController(animated: true)
    }
    override func navLeftBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
