//
//  LoginViewController.swift
//  robin8Swift
//
//  Created by RB8 on 2017/12/8.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit
class LoginViewController: BaseViewController,BaseVCDelegate,UITextFieldDelegate {
    var timer :Timer?
    //秒数
    var seconds = 60
    var entity:myEntity?
    
    
    @IBOutlet weak var codeBtn: UIButton!
    @IBOutlet weak var phoneText: UITextField!
    @IBOutlet weak var secretText: UITextField!
    @IBAction func getCode(_ sender: UIButton) {
        if phoneText.text != nil {
            if Validate.phoneNum(phoneText.text!).isRight == true {
                print(phoneText);
                YFLoadingProvider.request1(.GetCode(phone:phoneText.text!), completion: { (result) in
                    print(result)
                    self.codeBtnAction()
                   
                }) { (error) in

                }
//                RequestData.sendVerificationcode(phone: phoneText.text!, success: { (result) in
//                    print(result)
//                }, failture: { (error) in
//                    print(error)
//                })
            }else{
                let alert = UIAlertController.init(title: "提示", message: "手机号格式不对", preferredStyle: .alert)
                let action = UIAlertAction.init(title: "确定", style: .default, handler: { (acion) in

                })
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let alert = UIAlertController.init(title: "提示", message: "请输入手机号", preferredStyle: .alert)
            let action = UIAlertAction.init(title: "确定", style: .default, handler: { (acion) in

            })
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
 
    }
    func codeBtnAction() {
        begin()
    }
    @objc func changeSeconds() {
        switch seconds {
        case 1...60 :
            seconds = seconds - 1
            codeBtn.setTitle(String(describing: seconds), for: .normal)
        case  0:
            timerSuspend()
            seconds = 60
            codeBtn.setTitle("重新获取", for: .normal)
            codeBtn.backgroundColor = UIColor.init(red: 43/255.0, green: 219/255.0, blue: 226/255.0, alpha: 1.0)
            codeBtn.isEnabled = true
        default:
            return
        }
    }
    //开启
    func begin() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        seconds = 5
        codeBtn.isEnabled = false
        codeBtn.backgroundColor = UIColor.lightGray
        codeBtn.setTitle(String(seconds), for: .normal)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(changeSeconds), userInfo: nil, repeats: true)
    }
    //暂停
    func timerSuspend() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    //登录
    @IBAction func loginAction(_ sender: UIButton) {
        if secretText.text == nil {
            let alert = UIAlertController.init(title: "提示", message: "请输入验证码", preferredStyle: .alert)
            let action = UIAlertAction.init(title: "确定", style: .default, handler: { (acion) in
                
            })
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }else if phoneText.text == nil{
            let alert = UIAlertController.init(title: "提示", message: "请输入手机号", preferredStyle: .alert)
            let action = UIAlertAction.init(title: "确定", style: .default, handler: { (acion) in
                
            })
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        } else{
            YFLoadingProvider.request1(.Login(phone:phoneText.text!,code:secretText.text!), completion: { (result) in
                if result["error"] == 0{
                    let dic = result["kol"]
                    self.entity = dealData.loginData(jsonObject: dic)
                    LocalService.setRBLocalDataUserLoginIdWith(str: self.entity?.id)
                    LocalService.setRBLocalDataUserLoginNameWith(str: self.entity?.name)
                    LocalService.setRBLocalDataUserLoginPhoneWith(str: self.entity?.mobile_number)
                    LocalService.setRBLocalDataUserLoginAvatarWith(str: self.entity?.avatar_url)
                    LocalService.setRBLocalDataUserPrivateTokenWith(str: self.entity?.issue_token)
                    LocalService.setRBLocalDataUserLoginKolIdWith(str: self.entity?.kol_uuid)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    print(result)
                }
            }) { (error) in
                print(error)
            }
        }
       
    }
    override func navLeftBtnAction() {
        self.dismiss(animated: true) {
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.navTitle = "注册/登录"
        self.navView?.backgroundColor = UIColor.clear
        codeBtn.layer.cornerRadius = 15.0
        codeBtn.layer.masksToBounds = true
        self.phoneText.delegate = self
        self.secretText.delegate = self
        
        // Do any additional setup after loading the view.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        phoneText.resignFirstResponder()
        secretText.resignFirstResponder()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.view.frame = CGRect(x: 0, y: -100, width: KScreenWidth, height: KScreenHeight)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.view.frame = CGRect(x: 0, y: 0, width: KScreenWidth, height: KScreenHeight)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
