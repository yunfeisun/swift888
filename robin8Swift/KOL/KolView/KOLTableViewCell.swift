//
//  KOLTableViewCell.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/22.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit

class KOLTableViewCell: UITableViewCell {
    var backView: UIView?
    var backImageView: UIImageView?
    var titleLabel:UILabel?
    var timeLabel:UILabel?
    var centerLeftTLabel:UILabel?
    var centerLeftCLabel:UILabel?
    var centerRightTLabel:UILabel?
    var centerRightCLabel:UILabel?
    var bottomView:UIView?
    var bottomRightLabel:UILabel?
    var bottomLeftLabel:UILabel?
    var endLabel:UILabel?
    var robLabel:UILabel?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setUpUI()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    private  func setUpUI() {
        backView = UIView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height:200))
        backView?.backgroundColor = UIColor.white
        self.contentView.addSubview(backView!)
        //
        backImageView = UIImageView.init(frame: CGRect(x: 0, y: 5, width: KScreenWidth, height: 190))
        backView?.addSubview(backImageView!)
        //
        endLabel = UILabel.initFrom(frame: CGRect(x:(KScreenWidth - 60)/2, y: 25, width: 60, height: 20), title: "已结束", titleColor: UIColor.white, textAligment: .center, backColor: UIColor.withHex(hexString: SysColorRed), font: UIFont.systemFont(ofSize: 13))
        backImageView?.addSubview(endLabel!)
        //
        titleLabel = UILabel.init(frame: CGRect(x: 0, y:45, width: KScreenWidth, height: 25))
        titleLabel?.textColor = UIColor.blue
        titleLabel?.backgroundColor = UIColor.clear
        titleLabel?.textAlignment = .center
        backImageView?.addSubview(titleLabel!)
        //
        centerLeftTLabel = UILabel.initFrom(frame: CGRect(x: 40, y:(titleLabel?.bottom)!, width: 100, height:25), title: "活动剩余", titleColor: UIColor.white, textAligment:.center, backColor: UIColor.withHex(hexString: "000000", alpha: 0.6), font:UIFont.systemFont(ofSize: 15))
        backImageView?.addSubview(centerLeftTLabel!)
        //
        centerLeftCLabel = UILabel.initFrom(frame: CGRect(x: 40, y:(centerLeftTLabel?.bottom)!, width: 100, height:30), title: "1234567", titleColor: UIColor.withHex(hexString: SysColorBlack), textAligment: .center, backColor: UIColor.clear, font: UIFont.systemFont(ofSize: 15))
        backImageView?.addSubview(centerLeftCLabel!)
        //
        centerRightTLabel = UILabel.initFrom(frame: CGRect(x: KScreenWidth - 140, y:(titleLabel?.bottom)!, width: 100, height:30), title: "最多可赚", titleColor: UIColor.white, textAligment: .center, backColor: UIColor.withHex(hexString: "000000", alpha: 0.6), font: UIFont.systemFont(ofSize: 15))
        backImageView?.addSubview(centerRightTLabel!)
        //
        centerRightCLabel = UILabel.initFrom(frame: CGRect(x: KScreenWidth - 140, y:(centerRightTLabel?.bottom)!, width: 100, height:30), title: "1234567", titleColor: UIColor.withHex(hexString: SysColorBlack), textAligment: .center, backColor: UIColor.clear, font: UIFont.systemFont(ofSize: 15))
        backImageView?.addSubview(centerRightCLabel!)
        //
        bottomView = UIView.init(frame: CGRect(x: 0, y: 200 - 40, width: KScreenWidth, height: 40))
        bottomView?.backgroundColor = UIColor.withHex(hexString: SysColorBlack, alpha: 0.5)
        backImageView?.addSubview(bottomView!)
        //
        bottomRightLabel = UILabel.initFrom(frame: CGRect(x: 0, y: 0, width: KScreenWidth/2, height: 40), title: "即将赚:", titleColor: UIColor.white, textAligment: .right, backColor: UIColor.clear, font: UIFont.systemFont(ofSize: 15))
        bottomView?.addSubview(bottomRightLabel!)
        //
        bottomLeftLabel = UILabel.initFrom(frame: CGRect(x: KScreenWidth/2, y: 0, width: KScreenWidth/2, height: 40), title: " $ 5", titleColor: UIColor.withHex(hexString: SysColorYellow), textAligment: .left, backColor: UIColor.clear, font: UIFont.systemFont(ofSize: 15))
        bottomView?.addSubview(bottomLeftLabel!)
        //
        robLabel = UILabel.initFrom(frame: CGRect(x: (KScreenWidth - 60)/2, y: (titleLabel?.bottom)!, width: 60, height: 10), title:"已抢完", titleColor: UIColor.white, textAligment: .center, backColor: UIColor.clear, font: UIFont.systemFont(ofSize: 14))
        backImageView?.addSubview(robLabel!)
    }
    func setModelForCell(model:campainEntity) {
        if (model.campaign?.img_url) != nil {
            let url = URL(string: (model.campaign?.img_url)!)
            backImageView?.kf.setImage(with: url)
            titleLabel?.text = model.campaign?.brand_name!
            //总预算
            let allBudget = "￥" + (model.campaign?.per_action_budget)!
            //赚的钱
            let earn_money = model.earn_money!
            //
            let take_budget = model.campaign?.take_budget
            //
            let status = model.status
            //
            let per_budget_type = model.campaign?.per_budget_type
            
            
            self.endLabel?.isHidden = true
            
            let campainStatus = model.campaign?.status
            if campainStatus == "settled" || campainStatus == "executed" || campainStatus == "rejected"{
                self.endLabel?.isHidden = false
            }else{
                self.endLabel?.isHidden = true
            }
            self.bottomView?.isHidden = true
            self.bottomLeftLabel?.isHidden = true
            self.bottomRightLabel?.isHidden = true
            self.centerLeftTLabel?.isHidden = true
            self.centerLeftCLabel?.isHidden = true
            self.centerRightTLabel?.isHidden = true
            self.centerRightCLabel?.isHidden = true
            self.robLabel?.isHidden = true
            if status == "approved" || status == "rejected" || status == "finished" || status == "settled"{
                self.bottomView?.isHidden = false
                self.bottomRightLabel?.isHidden = false
                self.bottomLeftLabel?.isHidden = false
                if status == "rejected"{
                    self.bottomRightLabel?.text = "已赚"
                    self.bottomLeftLabel?.text = "￥0.0"
                }else if status == "settled"{
                    self.bottomRightLabel?.text = "已赚"
                    self.bottomLeftLabel?.text = "\(earn_money)"
                }else{
                    self.bottomRightLabel?.text = "即将赚"
                    self.bottomLeftLabel?.text = "\(earn_money)"
                }
            }else{
                self.centerLeftTLabel?.isHidden = false
                self.centerLeftCLabel?.isHidden = false
                self.centerRightTLabel?.isHidden = false
                self.centerRightCLabel?.isHidden = false
                self.centerLeftTLabel?.text = "活动剩余"
                self.centerLeftCLabel?.text = PublicAPI.getUIDateCompareNow(dateStr: (model.campaign?.deadline)!)
                if campainStatus == "settled" || campainStatus == "executed" || campainStatus == "rejected"{
                    self.robLabel?.text = "已抢完"
                    self.robLabel?.isHidden = false
                    self.robLabel?.isHidden = false
                    self.centerRightTLabel?.isHidden = true
                    self.centerRightCLabel?.isHidden = true
                    self.centerLeftTLabel?.isHidden = true
                    self.centerLeftCLabel?.isHidden = true
                }else{
                    self.centerLeftTLabel?.textColor = UIColor.withHex(hexString: SysColorYellow)
                    self.centerRightTLabel?.textColor = UIColor.withHex(hexString: SysColorYellow)
                    self.centerRightCLabel?.text = "\(String(describing: model.campaign?.budget))"
                }
                if per_budget_type == "recruit"{
                    self.centerRightTLabel?.text = "招募人数"
                    self.centerRightCLabel?.text = model.campaign?.max_action
                }
                if per_budget_type == "invite"{
                    self.centerRightTLabel?.text = "特邀人数"
                    self.centerRightCLabel?.text = model.campaign?.max_action
                }
            }
            if status == "approved" || status == "finished"{
                self.endLabel?.text = "正在结算"
            }else{
                self.endLabel?.text = "已结束"
            }
            //招募活动
            if per_budget_type == "recruit"{
                if status == "running"{
                    if PublicAPI.getUIDateCompareNow(dateStr: (model.campaign?.recruit_end_time)!) == "活动已结束"{
                        self.endLabel?.isHidden = false
                        self.endLabel?.backgroundColor = UIColor.withHex(hexString: SysColorRed)
                        self.endLabel?.text = "报名结束"
                    }else{
                        self.endLabel?.isHidden = false
                        self.endLabel?.backgroundColor = UIColor.withHex(hexString: SysColorRed)
                        self.endLabel?.text = "报名中"
                        
                    }
                }
                if status == "applying"{
                    self.endLabel?.isHidden = false
                    self.endLabel?.backgroundColor = UIColor.withHex(hexString: SysColorRed)
                    self.endLabel?.text = "已报名"
                    
                }
            }
        
        
        }
        
        
    }
}
