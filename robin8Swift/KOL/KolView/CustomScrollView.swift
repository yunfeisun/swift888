//
//  CustomScrollView.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/13.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit

class CustomScrollView: UIView,UIScrollViewDelegate {
    var scrollView = UIScrollView()
    let ScreenWidth = UIScreen.main.bounds.size.width
    
    
    
    func addImageView() {
        for index in 0...4 {
            let imageView = UIImageView(image: UIImage(named:"tabbar_0_h"))
            imageView.frame = CGRect(x: CGFloat(index) * ScreenWidth, y: 0, width: ScreenWidth, height: 200)
            scrollView.addSubview(imageView)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        scrollView.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: frame.size.height)
        scrollView.backgroundColor = UIColor.white
        
        scrollView.contentSize = CGSize(width: ScreenWidth * 5, height: 200)
        scrollView.contentInset = UIEdgeInsetsMake(20, 20, 20, 20)
        scrollView.alwaysBounceHorizontal = true
        scrollView.alwaysBounceVertical = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.isScrollEnabled = true
        scrollView.isDirectionalLockEnabled = true
        //  控制控件遇到边框是否反弹
        scrollView.bounces = false
        scrollView.delegate = self
        self.addSubview(scrollView)
        self.addImageView()
    }
    //代理方法
    //已经开始滚动
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    //将要开始拖拽
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
    }
    //将要结束拖拽，手指已拖动过view并准备离开手指的那一刻，注意：当属性isPagingEnabled为YES时，此函数不被调用
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
    }
    //已经结束拖拽，手指刚离开view的那一刻
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
    }
    //view将要开始减速，view滑动之后有惯性
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
    }
    //view已经停止滚动
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    //view的缩放
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
    }
    //有动画时调用
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
