//
//  KolHeaderView.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/14.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit
import Kingfisher
class KolHeaderView: UITableViewHeaderFooterView,UIScrollViewDelegate {
    var scrollView = UIScrollView()
    var pageC :UIPageControl?
    var timer :Timer?
    var scArray = [String]()
    
    
    let ScreenWidth = UIScreen.main.bounds.size.width
    @objc func changeOffSet() {
        let offsetX = scrollView.contentOffset.x
        UIView.animate(withDuration: 0.5, animations: {
            self.scrollView.contentOffset = CGPoint(x: offsetX + self.ScreenWidth, y: 0)
        }) { (boolValue:Bool) in
            if self.scrollView.contentOffset.x == 0 {
                self.scrollView.contentOffset = CGPoint(x: self.ScreenWidth * CGFloat(self.scArray.count - 2), y: 0)
            }else if self.scrollView.contentOffset.x == self.scrollView.contentSize.width - self.ScreenWidth{
                self.scrollView.contentOffset = CGPoint(x: KScreenWidth, y: 0)
            }
            self.pageC?.currentPage = Int(self.scrollView.contentOffset.x/self.ScreenWidth) - 1
        }
    }
    func addImageView(WithImagearr ImageArr:NSArray) {
        scArray = ImageArr as! [String]
        scArray.insert(ImageArr.lastObject as Any as! String, at: 0)
        scArray.append(ImageArr[0] as! String)
        for view in scrollView.subviews {
            if view is UIImageView{
                view.removeFromSuperview()
            }
        }
        for index in 0...(scArray.count)-1 {
            let imageView = UIImageView(image: UIImage(named:"tabbar_0_h"))
            imageView.frame = CGRect(x: CGFloat(index) * ScreenWidth, y: 0, width: ScreenWidth, height: 200)
           
            let url = URL(string: scArray[index])
            imageView.kf.setImage(with: url)
            scrollView.addSubview(imageView)
        }
        
        scrollView.contentSize = CGSize(width: ScreenWidth * 6, height: 200)
        scrollView.contentOffset = CGPoint(x: KScreenWidth, y: 0)
        pageC?.numberOfPages = ImageArr.count
        pageC?.currentPage = 0
        pageC?.frame = CGRect(x: (KScreenWidth - 200)/2, y: 200 - 50, width: 200, height: 50)
        timerSuspend()
        begin()
    }
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        scrollView.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: 200)
        scrollView.backgroundColor = UIColor.white
        
        scrollView.alwaysBounceHorizontal = false
        scrollView.alwaysBounceVertical = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.isScrollEnabled = true
        scrollView.isDirectionalLockEnabled = true
        //  控制控件遇到边框是否反弹
        scrollView.bounces = false
        scrollView.delegate = self
        self.addSubview(scrollView)
        pageC = UIPageControl.init(frame: CGRect(x: (ScreenWidth - 200)/2, y: 200 - 50, width: 200, height: 50))
        pageC?.backgroundColor = UIColor.clear
        pageC?.pageIndicatorTintColor = UIColor.red
        pageC?.currentPageIndicatorTintColor = UIColor.blue
        pageC?.currentPage = 0
        pageC?.numberOfPages = Int(scrollView.contentSize.width/KScreenWidth)
        self.addSubview(pageC!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x < KScreenWidth {
//            scrollView.scrollRectToVisible(CGRect(x: scrollView.contentSize.width - 2 * ScreenWidth, y: 0, width: ScreenWidth, height: 200), animated: false)
            scrollView.contentOffset = CGPoint(x: ScreenWidth * CGFloat(scArray.count - 2), y: 0)
        }else if scrollView.contentOffset.x > scrollView.contentSize.width - 2*ScreenWidth{
//            scrollView.scrollRectToVisible(CGRect(x: ScreenWidth, y: 0, width: ScreenWidth, height: 200), animated: false)
            scrollView.contentOffset = CGPoint(x: KScreenWidth, y: 0)
        }
        pageC?.currentPage = Int(scrollView.contentOffset.x/ScreenWidth) - 1
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        timerSuspend()
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        begin()
    }
    
    //开启
    func begin() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(changeOffSet), userInfo: nil, repeats: true)
        RunLoop.main.add(timer!, forMode:.commonModes)
    }
    //暂停
    func timerSuspend() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    //
//    func timerRun(runTimer:Timer) {
//        let offsetX = scrollView.contentOffset.x
//        UIView.animate(withDuration: 0.5, animations: {
//            self.scrollView.contentOffset = CGPoint(x:offsetX + KScreenWidth,y:0)
//        }) { (boolValue:Bool) in
//            if self.scrollView.contentOffset.x == 0 {
//                //            scrollView.scrollRectToVisible(CGRect(x: scrollView.contentSize.width - 2 * ScreenWidth, y: 0, width: ScreenWidth, height: 200), animated: false)
//                self.scrollView.contentOffset = CGPoint(x: self.ScreenWidth * CGFloat(self.scArray.count - 2), y: 0)
//            }else if self.scrollView.contentOffset.x == self.scrollView.contentSize.width - self.ScreenWidth{
//                //            scrollView.scrollRectToVisible(CGRect(x: ScreenWidth, y: 0, width: ScreenWidth, height: 200), animated: false)
//                self.scrollView.contentOffset = CGPoint(x: KScreenWidth, y: 0)
//            }
//            self.pageC?.currentPage = Int(self.scrollView.contentOffset.x/self.ScreenWidth) - 1
//        }
//
//    }
}
