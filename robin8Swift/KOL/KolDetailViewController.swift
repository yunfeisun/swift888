//
//  KolDetailViewController.swift
//  robin8Swift
//
//  Created by RB8 on 2017/12/6.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit
class KolDetailViewController: BaseViewController,BaseVCDelegate,UIScrollViewDelegate {
    
    var kolId:String?
    var scrollviewn:UIScrollView?
    var bgImg: UIImageView?
    var detailModel:kolDetailModel?
    
    func NavLeftBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func NavRightBtnAction() {
        
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = UIColor.white
        loadKOLView()
        self.delegate = self
        self.navTitle = "KOL详情"
        self.navRightTitle = "collect"
        self.navView?.backgroundColor = UIColor.clear
        requestData()
    }
    func requestData() {
        if kolId != nil {
//            YFLoadingProvider.request(, completion: <#T##((JSON) -> Void)?##((JSON) -> Void)?##(JSON) -> Void#>, failure: <#T##((Error) -> Void)##((Error) -> Void)##(Error) -> Void#>)
            YFLoadingProvider.request1(.CampainDetailUrl(id:kolId!), completion: { (result) in
                print(String(describing: result) + "hehe")
            }, failure: { (error) in
                
            })
        }
        
    }
    func reloaData() {
        let imgUrl = URL(string: (detailModel?.avatar_url)!)
        bgImg?.kf.setImage(with: imgUrl)
    }
    func loadKOLView() {
        scrollviewn = UIScrollView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: KScreenHeight - 45.0))
        scrollviewn?.showsVerticalScrollIndicator = false
        scrollviewn?.showsHorizontalScrollIndicator = false
        scrollviewn?.delegate = self
        self.view.addSubview(scrollviewn!)
        //
        bgImg = UIImageView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: KScreenWidth * 730/750))
        bgImg?.contentMode = .scaleAspectFill
        bgImg?.clipsToBounds = true
        scrollviewn?.addSubview(bgImg!)
        //
        let InfluenceBtn = UIButton.init(frame: CGRect(x: 0, y: (bgImg?.bottom)!, width:KScreenWidth, height: 67))
        InfluenceBtn.setTitle("查看TA的影响力", for: .normal)
        InfluenceBtn.setTitleColor(UIColor.black, for: .normal)
        InfluenceBtn.backgroundColor = UIColor.red
        InfluenceBtn.addTarget(self, action: #selector(lookInfluence), for:.touchUpInside)
        scrollviewn?.addSubview(InfluenceBtn)
    }
    
    @objc func lookInfluence() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
