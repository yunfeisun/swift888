//
//  KOLViewController.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/10.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit
import SwiftyJSON
import ESPullToRefresh
import Moya
import Alamofire
class KOLViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,ESRefreshProtocol,BaseVCDelegate{
    func refreshAnimationBegin(view: ESRefreshComponent) {
        
    }
    
    func refreshAnimationEnd(view: ESRefreshComponent) {
        
    }
    
    func refresh(view: ESRefreshComponent, progressDidChange progress: CGFloat) {
        
    }
    
    func refresh(view: ESRefreshComponent, stateDidChange state: ESRefreshViewState) {
        
    }
    
    let ScreenWidth:CGFloat = UIScreen.main.bounds.size.width
    var dataList:[campainEntity] = []
    var bannerList:[kolBannerModel] = []
    var MyTableView: UITableView?
    var page:Int = 1
    var totalPage:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navTitle = "首页"
        self.delegate = self
        self.navLeftTitle = nil
        self.navigationController?.navigationBar.isHidden = true
        
        self.view.backgroundColor = UIColor.white
        self.automaticallyAdjustsScrollViewInsets = false
         MyTableView = UITableView.init(frame: CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - 64 - 49), style: UITableViewStyle.grouped)
        MyTableView?.delegate = self
        MyTableView?.dataSource = self
        MyTableView?.register(KolHeaderView.classForCoder(), forHeaderFooterViewReuseIdentifier: "header")
        MyTableView?.register(KOLTableViewCell.classForCoder(), forCellReuseIdentifier: "KOLTableViewCell")
        self.view.addSubview(MyTableView!)
        addRefresh()
        var dic = [String:String]()
        dic["page"] = "1"
        dic["with_kol_announcement"] = "Y"
        dic["order"] = "order_by_created"
        requestData()
    }
    func requestData() {
        //请求数据
        YFLoadingProvider.request1(.HomeUrl(page: 1, banner: "Y", order: "order_by_hot"), completion: { (result) in
            let arr = result["kol_announcements"].arrayValue
            self.bannerList.removeAll()
            for index in 0...arr.count-1{
                let jsonData = JSON(arr[index])
                let bannerModel = kolBannerModel.init(jsonData: jsonData)
                self.bannerList.append(bannerModel)
            }
            self.MyTableView?.reloadData()
        }, failure:{ error in
            print(error)
        })
        YFLoadingProvider.request1(.GetCampainData(status: "all", title: "", message: "", page: String(self.page)), completion: { (result) in
            if self.page == 1 {
                self.dataList.removeAll()
                self.MyTableView?.es.stopPullToRefresh()
            }
            if result["total_pages"].intValue >= self.page{
                self.dataList = dealData.campainData(jsonObject: result, backArray: self.dataList) as! [campainEntity]
                self.MyTableView?.reloadData()
                self.MyTableView?.es.stopPullToRefresh()
                self.MyTableView?.es.stopLoadingMore()
            }else{
                self.MyTableView?.es.noticeNoMoreData()
            }
        }) { (error) in
            
        }
    }
    func addRefresh() {
        self.MyTableView?.es.addPullToRefresh {
            self.page = 1
            self.requestData()
        }
        self.MyTableView?.es.addInfiniteScrolling {
            self.page = self.page + 1
            if self.dataList.count % 10 != 0{
                self.MyTableView?.es.stopLoadingMore()
            }else{
                self.requestData()
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell:KOLTableViewCell = tableView.dequeueReusableCell(withIdentifier: "KOLTableViewCell", for: indexPath) as! KOLTableViewCell
        if cell.isEqual(nil) {
            cell = KOLTableViewCell(style: .default, reuseIdentifier: "cell")
        }
        cell.selectionStyle = .none
        
        let model:campainEntity = self.dataList[indexPath.row]
        cell.setModelForCell(model: model)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let model:campainEntity = self.dataList[indexPath.row]
        let detailVC = campainDetailController()
        detailVC.hidesBottomBarWhenPushed = true
        detailVC.campainID = model.campaign?.id
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as! KolHeaderView
        headView.frame = CGRect(x: 0, y: 0, width: KScreenWidth, height: 200)
        if self.bannerList.count > 0 {
            var arr = [String]()
            for index in 0...self.bannerList.count - 1{
                let kolbannerModel = self.bannerList[index] 
                arr.append((kolbannerModel.cover_url)!)
            }
            headView.addImageView(WithImagearr: arr as NSArray)
        }
        return headView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
