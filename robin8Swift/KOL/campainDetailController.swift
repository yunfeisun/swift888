//
//  campainDetailController.swift
//  robin8Swift
//
//  Created by RB8 on 2018/3/2.
//  Copyright © 2018年 robin8. All rights reserved.
//

import UIKit
import SnapKit
class campainDetailController: BaseViewController,BaseVCDelegate,UIScrollViewDelegate,UIWebViewDelegate {
    var scrollviewn:UIScrollView?
    var webView:UIWebView?
    var campainID:String?
    var activityEntity:campainDetailEntity?
    var footerView:UIView?
    var footerBtn : UIButton?
    var footerRightBtn : UIButton?
    var footerLineLabel : UILabel?
    var tagLabel : UILabel?
    var tagTextLabel : UILabel?
    var tagTimeLabel : UILabel?
    var kolLabel : UILabel?
    var tempDate : Date?
    var updateTimer : Timer?
    var webViewNV : UIView?
    
    
    override func navLeftBtnAction() {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.navTitle = "Robin8"
        self.navigationController?.navigationBar.isHidden = true
        self.navView?.backgroundColor = UIColor.withHex(hexString: "000000", alpha: 0.2)
        getActivityData()
        // Do any additional setup after loading the view.
    }
    func getActivityData() {
        YFLoadingProvider.request1(.GetDetailCampainData(invitee_page: "1", activityID: self.campainID!), completion: { (result) in
            print(result)
            if "\(result["error"])" == "0"{
              self.activityEntity = dealData.campaignDetail(jsonObject: result)
              self.initUI()
              self.updateFooterView()
              self.loadWebView()
            }
        }) { (error) in
            
        }
    }
    func updateFooterView() {
        footerView?.removeFromSuperview()
        footerView = nil
        footerView = UIView.init(frame: CGRect(x: 0, y: KScreenHeight - 45.0, width: KScreenWidth, height: 45))
        self.view.addSubview(footerView!)
        //
        footerBtn = UIButton.init(frame: CGRect(x: 0, y: 0, width: (footerView?.width)!,height: (footerView?.height)!))
        footerBtn?.titleLabel?.font = UIFont.font_cu(s: 15)
        footerBtn?.setTitleColor(UIColor.withHex(hexString:"ffffff"), for: .normal)
        footerView?.addSubview(footerBtn!)
        //
        footerRightBtn = UIButton.init(frame: CGRect(x: (footerView?.width)!/2, y: 0, width: (footerView?.width)!/2, height: (footerView?.height)!))
        footerRightBtn?.titleLabel?.font = UIFont.font_cu(s: 15)
        footerRightBtn?.setTitleColor(UIColor.withHex(hexString: "ffffff"), for: .normal)
        footerView?.addSubview(footerRightBtn!)
        //
        footerLineLabel = UILabel.initFrom(frame: CGRect(x: (footerView?.width)!/2.0 - 0.5, y: 10.0, width: 1, height: 45 - 20), title: nil, titleColor: nil, textAligment: .center, backColor: UIColor.withHex(hexString: "ffffff"), font: nil)
        footerView?.addSubview(footerLineLabel!)
        //
        self.updateText()
        //
        footerBtn?.isEnabled = false
        footerBtn?.frame = CGRect(x: 0, y: 0, width: KScreenWidth, height: 45.0)
        footerRightBtn?.frame = CGRect(x: KScreenWidth/2.0, y: 0, width: KScreenWidth/2.0, height: 45.0)
        footerRightBtn?.isHidden = true
        footerLineLabel?.isHidden = true
        //
        let status = String(format:"%@",(activityEntity?.status)!)
        if status == "pending" {
            footerBtn?.setTitle("活动未开始", for: .normal)
            footerBtn?.backgroundColor = UIColor.withHex(hexString:SysColorSubGray)
            footerBtn?.isEnabled = false
        }else if status == "running"{
            footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
            footerBtn?.isEnabled = true
            footerBtn?.setTitle("分享赚收益", for: .normal)
            footerBtn?.addTarget(self, action: #selector(footerShareApproveBtnAction), for: .touchUpInside)
            return
        }else if status == "approved" || status == "finished"{
            if activityEntity?.screenshot?.count == 0{
                if status == "finished"{
                    if Int((activityEntity?.start_upload_screenshot)!) == 1{
                        if (activityEntity?.can_upload_screenshot)! == true{
                            footerBtn?.setTitle("上传截图", for: .normal)
                            footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
                            footerBtn?.isEnabled = true
                            footerBtn?.addTarget(self, action: #selector(footerUploadBtnAction), for: .touchUpInside)
                            footerBtn?.frame = CGRect(x: 0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                            footerRightBtn?.frame = CGRect(x: KScreenWidth/2.0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                            footerRightBtn?.isHidden = false
                            footerLineLabel?.isHidden = false
                            footerRightBtn?.setTitle("再次分享", for: .normal)
                            footerLineLabel?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
                            footerRightBtn?.addTarget(self, action: #selector(footerShareApproveBtnAction), for: .touchUpInside)
                            return
                        }else{
                            footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
                            footerBtn?.isEnabled = false
                            footerBtn?.setTitle("上传截图时间已过", for: .normal)
                            footerBtn?.frame = CGRect(x: 0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                            //
                            footerRightBtn?.frame = CGRect(x: KScreenWidth/2.0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                            footerRightBtn?.isHidden = false
                            footerRightBtn?.setTitle("再次分享", for: .normal)
                            footerRightBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
                            footerRightBtn?.addTarget(self, action: #selector(footerShareApproveBtnAction), for: .touchUpInside)
                            return
                        }
                    }else{
                        let tempTime = (activityEntity?.upload_interval_time![2])! + (activityEntity?.upload_interval_time![3])!
                        if tempTime == "0:0"{
                            footerBtn?.setTitle("未知状态", for: .normal)
                        }else{
                            let formatter = DateFormatter.init()
                            formatter.dateFormat = "m:s"
                            tempDate = formatter.date(from: tempTime)
                            updateTimer?.invalidate()
                            updateTimer = nil
                            updateTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(beginUpload), userInfo: nil, repeats: true)
                            let temp = "\(String(describing: activityEntity?.upload_interval_time![2]))分\(String(describing: activityEntity?.upload_interval_time![3]))秒 后请上传截图"
                            footerBtn?.setTitle(temp, for: .normal)
                        }
                        footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorSubGray)
                        footerBtn?.isEnabled = false
                        //
                        footerBtn?.frame = CGRect(x: 0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                        footerRightBtn?.frame = CGRect(x: KScreenWidth/2.0, y: 0, width: KScreenWidth/2.0, height: 45)
                        footerRightBtn?.isHidden = false
                        footerRightBtn?.setTitle("再次分享", for: .normal)
                        footerRightBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
                        footerRightBtn?.addTarget(self, action: #selector(footerShareApproveBtnAction), for:.touchUpInside)
                        return
                    }
                }else{
                    if  (activityEntity?.can_upload_screenshot)! == true{
                        footerBtn?.setTitle("上传截图", for: .normal)
                        footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
                        footerBtn?.isEnabled = true
                        footerBtn?.addTarget(self, action: #selector(footerUploadBtnAction), for: .touchUpInside)
                        //
                        footerBtn?.frame = CGRect(x: 0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                        footerRightBtn?.frame = CGRect(x: KScreenWidth/2.0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                        footerRightBtn?.isHidden = false
                        footerLineLabel?.isHidden = false
                        footerRightBtn?.setTitle("再次分享", for: .normal)
                        footerRightBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
                        footerRightBtn?.addTarget(self, action: #selector(footerShareApproveBtnAction), for: .touchUpInside)
                        return
                    }else{
                        let tempTime = "\(String(describing: activityEntity?.upload_interval_time![2]))" + "\(String(describing: activityEntity?.upload_interval_time![3]))"
                        if tempTime == "0:0"{
                            footerBtn?.setTitle("未知状态", for: .normal)
                        }else{
                            let formatter = DateFormatter()
                            formatter.dateFormat = "m:s"
                            tempDate = formatter.date(from: tempTime)
                            updateTimer?.invalidate()
                            updateTimer = nil
                            updateTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(beginUpload), userInfo: nil, repeats: true)
                            let temp = "\((activityEntity?.upload_interval_time![2])!)分\((activityEntity?.upload_interval_time![3])!)秒 后请上传截图"
                            footerBtn?.setTitle(temp, for: .normal)
                        }
                        footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorSubGray)
                        footerBtn?.isEnabled = false
                        footerLineLabel?.isHidden = true
                        //
                        footerBtn?.frame = CGRect(x: 0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                        footerRightBtn?.frame = CGRect(x: KScreenWidth/2.0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                        footerRightBtn?.isHidden = false
                        footerRightBtn?.setTitle("再次分享", for: .normal)
                        footerRightBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
                        footerRightBtn?.addTarget(self, action: #selector(footerShareApproveBtnAction), for: .touchUpInside)
                        return
                    }
                    
                }
            }else{
                var temp = ""
                let img_status = activityEntity?.img_status
                if img_status == "passed"{
                    temp = "审核通过"
                    footerRightBtn?.setTitle("再次分享", for: .normal)
                    footerRightBtn?.addTarget(self, action: #selector(footerShareApproveBtnAction), for: .touchUpInside)
                    footerBtn?.setTitle("\(temp)查看截图", for: .normal)
                }else if img_status == "pending"{
                    temp = "审核中"
                    footerRightBtn?.setTitle("再次分享", for: .normal)
                    footerRightBtn?.addTarget(self, action: #selector(footerShareApproveBtnAction), for: .touchUpInside)
                    footerBtn?.setTitle("再次上传", for: .normal)
                }else if img_status == "rejected"{
                    temp = "审核拒绝"
                    footerRightBtn?.setTitle("重新上传", for: .normal)
                    footerRightBtn?.addTarget(self, action: #selector(footerUploadBtnAction), for: .touchUpInside)
                    footerBtn?.setTitle("\(temp) 查看原因", for: .normal)
                }
                footerBtn?.frame = CGRect(x: 0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
                footerBtn?.isEnabled = true
                footerBtn?.addTarget(self, action: #selector(footerSeeBtnAction), for: .touchUpInside)
                footerLineLabel?.isHidden = false
                footerRightBtn?.frame = CGRect(x: KScreenWidth/2.0, y: 0, width: KScreenWidth/2.0, height: 45.0)
                footerRightBtn?.isHidden = false
                footerRightBtn?.isEnabled = true
                footerRightBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
            }
        }else if status == "settled"{
            footerBtn?.setTitle("活动已完成", for:.normal)
            footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
            footerBtn?.isEnabled = false
            return
        }else if status == "rejected"{
            footerBtn?.setTitle("活动审核失败", for: .normal)
            footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorSubGray)
            footerBtn?.isEnabled = false
            return
        }else if status == "missed"{
            footerBtn?.setTitle("活动已错失", for: .normal)
            footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorBlue)
            footerBtn?.isEnabled = false
            return
        }else if status == "countdown"{
            footerBtn?.setTitle("活动即将开始", for: .normal)
            footerBtn?.backgroundColor = UIColor.withHex(hexString: SysColorSubGray)
            footerBtn?.isEnabled = false
            return
        }
    }
  @objc  func footerSeeBtnAction() {
    let img_status = activityEntity?.img_status
    if activityEntity?.reject_reason?.count == 0 {
        let alert = UIAlertController.init(title: "图片审核不通过", message: activityEntity?.reject_reason, preferredStyle: .alert)
        let action = UIAlertAction.init(title: "截图参考", style: .default, handler: { (alert) in
            
        })
        let action1 = UIAlertAction.init(title: "查看截图", style: .default, handler: { (alert) in
            
        })
        alert.addAction(action)
        alert.addAction(action1)
        self.navigationController?.pushViewController(alert, animated: true)
    }
    }
    @objc func beginUpload() {
        let formatter = DateFormatter.init()
        formatter.dateFormat = "m:s"
        let zeroDate = formatter.date(from: "0:0")
        if tempDate == zeroDate {
            updateTimer?.invalidate()
            updateTimer = nil
            self.getActivityData()
        }else{
            tempDate = Date(timeInterval: -1, since: tempDate!)
            var temp = formatter.string(from: tempDate!)
            temp = temp.replacingOccurrences(of: ":", with: "分")
            let str = "\(temp)秒 后请上传截图"
            footerBtn?.setTitle(str, for: .normal)
        }
    }
    @objc  func footerUploadBtnAction() {
        
    }
    @objc  func footerShareApproveBtnAction() {
        
    }
    func updateText() {
        let per_budget_type = activityEntity?.campaign?.per_budget_type
        var per_action_budget = String.init(format: "%.2f", Float((activityEntity?.campaign?.per_action_budget)!)!)
        let status = activityEntity?.status
        let avail_click = activityEntity?.avail_click
        
        let earn_money = String(format:"%.2f",Float((activityEntity?.earn_money)!)!)
        print(earn_money)
        if per_budget_type == "click" {
            tagLabel?.text = "根据你的历史表现，你可单次点击转到￥\(per_action_budget)"
        }else if per_budget_type == "cpt"{
            tagLabel?.text = "根据你的历史表现，你可单次任务赚到￥\(per_action_budget)"
        }else if per_budget_type == "cpa" || per_budget_type == "simple_cpi"{
            
        }else{
            tagLabel?.text = "根据你的历史表现,你可单次转发赚到￥\(per_action_budget)"
        }
        if per_budget_type == "click" {
            tagTextLabel?.text = "分享后好友点击此文章立即获得报酬"
        }else if per_budget_type == "cpa"{
            
        }else if per_budget_type == "cpt"{
            tagTextLabel?.text = "分享后完成指定任务立即获得报酬"
        }else if per_budget_type == "simple_cpi"{
            tagTextLabel?.text = "分享后下载指定APP立即获得报酬"
        }else{
            tagTextLabel?.text = "分享此文章立即获得报酬"
        }
        tagTextLabel?.frame = CGRect(x: 0, y: 0, width: KScreenWidth - 2*20.0, height: 64.0)
        tagTimeLabel?.isHidden = true
        if per_budget_type == "click" {
            if status == "pending" || status == "running"{
                tagTimeLabel?.isHidden = false
                tagTextLabel?.frame = CGRect(x: 0, y: (64.0 - 18.0 - 15.0 - 5.0)/2.0, width: KScreenWidth - 2*20.0, height: 18.0)
                tagTimeLabel?.text = PublicAPI.getUIDateCompareNow(dateStr: (activityEntity?.campaign?.deadline)!)
            }else if status == "approved" || status == "finished"{
                tagTextLabel?.text = "已获得\(avail_click ?? "0.0")次点击,即将赚￥\(earn_money)"
            }else if status == "settled" || status == "missed" || status == "rejected"{
                tagTextLabel?.text = "已获得\(avail_click ?? "0.0")次点击,已赚￥\(earn_money)"
            }
        }else{
            if status == "pending" || status == "running"{
                tagTimeLabel?.isHidden = false
                tagTextLabel?.frame = CGRect(x: 0, y: (64.0 - 18.0 - 15.0 - 5.0)/2.0, width: KScreenWidth - 2*20.0, height: 18.0)
                tagTimeLabel?.text = PublicAPI.getUIDateCompareNow(dateStr: (activityEntity?.campaign?.deadline)!)
            }else if status == "approved" || status == "finished"{
                tagTextLabel?.text = "活动进行中,即将赚￥\(earn_money)"
            }else if status == "settled" || status == "missed" || status == "rejected"{
                tagTextLabel?.text = "已完成,已赚￥\(earn_money)"
            }
        }
        
    }
    func loadWebView() {
        webViewNV = UIView.init()
        webViewNV?.backgroundColor = UIColor.white
        scrollviewn?.addSubview(webViewNV!)
        //
        let navCenterLabel = UILabel.init()
        navCenterLabel.text = "推广内容详情"
        navCenterLabel.font = UIFont.font_cu(s: 17)
        navCenterLabel.textColor = UIColor.withHex(hexString: SysColorBlack)
        navCenterLabel.isUserInteractionEnabled = true
        navCenterLabel.textAlignment = .center
        webViewNV?.addSubview(navCenterLabel)
        //
        webView = UIWebView.init()
        webView?.delegate = self
        webView?.scalesPageToFit = true
        scrollviewn?.addSubview(webView!)
        //
        webViewNV?.snp.makeConstraints({ (make) in
            make.top.equalTo(KScreenHeight)
            make.width.equalTo(KScreenWidth)
            make.height.equalTo(navHeight)
        })
        //
        webView?.snp.makeConstraints({ (make) in
            make.top.equalTo((webViewNV?.snp.bottom)!)
            make.width.equalTo(KScreenWidth)
            make.height.equalTo(KScreenHeight - navHeight - 45)
            make.bottom.equalTo((scrollviewn?.snp.bottom)!)
        })
        //
        navCenterLabel.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        let url = activityEntity?.campaign?.url?.replacingOccurrences(of: "#wechat_redirect", with: "")
        webView?.loadRequest(URLRequest.init(url: URL.init(string: url!)!))
        
    }
    func initUI(){
        scrollviewn = UIScrollView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: KScreenHeight))
        scrollviewn?.delegate = self
        scrollviewn?.showsVerticalScrollIndicator = false
        scrollviewn?.showsHorizontalScrollIndicator = false
        scrollviewn?.isPagingEnabled = true
        scrollviewn?.bounces = true
        scrollviewn?.contentSize = CGSize(width: KScreenWidth, height: KScreenHeight * 2)
        self.view.addSubview(scrollviewn!)
        //
        bgIv = UIImageView.init(frame:CGRect(x: 0, y: 0, width: KScreenWidth, height: KScreenWidth * 8.0/16.0))
        let url = URL.init(string: (activityEntity?.campaign?.img_url)!)
        bgIv?.kf.setImage(with: url)
        scrollviewn?.addSubview(bgIv!)
        //
        let bgLabel = UILabel.init(frame:(bgIv?.bounds)!)
        bgLabel.backgroundColor = SysColorCover
        bgIv?.addSubview(bgLabel)
        //
        let titleLabel = UILabel.initFrom(frame: CGRect(x: 16, y: (bgIv?.bottom)! + 45.0, width: KScreenWidth - 32, height: 18), title: activityEntity?.campaign?.name, titleColor: UIColor.withHex(hexString: SysColorBlack, alpha: 1), textAligment: .center, backColor: UIColor.clear, font:UIFont.systemFont(ofSize: 14))
        scrollviewn?.addSubview(titleLabel!)
        //
        let subTitleLabel = UILabel.initFrom(frame: CGRect(x: 0, y: (titleLabel?.bottom)! + 6.0, width: KScreenWidth, height: 13), title: activityEntity?.campaign?.brand_name, titleColor: UIColor.withHex(hexString: SysColorBlack), textAligment: .center, backColor: UIColor.clear, font: UIFont.font(s: 11.0))
        scrollviewn?.addSubview(subTitleLabel!)
        //
        let timeLabel = UILabel.initFrom(frame: CGRect(x: 0, y:(subTitleLabel?.bottom)! + 6.0, width: KScreenWidth, height: 13), title: "", titleColor: UIColor.withHex(hexString: SysColorBlack), textAligment:.center, backColor: UIColor.clear, font: UIFont.font(s: 11.0))
        scrollviewn?.addSubview(timeLabel!)
        //
        let contentTV = UITextView.init(frame: CGRect(x: 20, y: (timeLabel?.bottom)! + 5.0, width: KScreenWidth - 20.0, height: 200))
        contentTV.font = UIFont.font(s: 13)
        contentTV.text = activityEntity?.campaign?.description
        contentTV.isEditable = false
        contentTV.textAlignment = .center
        contentTV.textColor = UIColor.withHex(hexString: SysColorBlack)
        scrollviewn?.addSubview(contentTV)
        //
        let lineLabel = UILabel.initFrom(frame: CGRect(x: 0, y:KScreenHeight - 35.0 - 64.0 - 35.0 - 60.0 - 26 - 0.5, width: KScreenWidth, height: 0.5), title: "", titleColor: UIColor.clear, textAligment: .center, backColor: SysColorCover, font:nil)
        scrollviewn?.addSubview(lineLabel!)
        //
        tagLabel = UILabel.initFrom(frame:CGRect(x: 0, y:KScreenHeight - 35.0 - 64.0 - 35.0 - 60.0 - 26 - 0.5, width: KScreenWidth, height: 0.5) , title: nil, titleColor: UIColor.withHex(hexString: SysColorBlack), textAligment: .center, backColor: UIColor.clear, font:UIFont.font(s: 15))
        scrollviewn?.addSubview(tagLabel!)
        //
        let tagView = UIImageView.init(frame: CGRect(x: 20, y: KScreenHeight - 64.0 - 20.0 - 60.0 - 26, width: KScreenWidth - 2*20, height: 64.0))
        tagView.backgroundColor = UIColor.withHex(hexString: SysColorSubBlack)
        scrollviewn?.addSubview(tagView)
        //
        tagTextLabel = UILabel.initFrom(frame: CGRect(x:0, y:(64 - 18.0 - 15.0 - 5.0)/2.0,width: KScreenWidth - 2*20,height:18.0), title: "", titleColor: UIColor.withHex(hexString: "ffffff"), textAligment: .center, backColor: UIColor.clear, font: UIFont.font_cu(s: 15))
        tagView.addSubview(tagTextLabel!)
        //
        tagTimeLabel = UILabel.initFrom(frame: CGRect(x:0, y:(tagTextLabel?.bottom)! + 5.0,width: KScreenWidth - 2 * 20,height:15), title: "", titleColor: UIColor.withHex(hexString: SysColorYellow), textAligment: .center, backColor: UIColor.clear, font: UIFont.font(s: 11.0))
        tagView.addSubview(tagTimeLabel!)
        //
        let kolBtn = UIButton.initfrom(frame: CGRect(x:tagView.left, y:KScreenHeight - 10.0 - 60.0 - 26,width: KScreenWidth - 2 * 20,height:60), title: "", hltitle: "", backColor: UIColor.clear, titleColor: UIColor.clear, titleFont: nil, image: nil)
        kolBtn?.addTarget(self, action: #selector(campainDetailController.kolBtnAction), for: .touchUpInside)
        scrollviewn?.addSubview(kolBtn!)
        
        //
        kolBtn?.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(KScreenHeight - 45 - 60)
            make.left.right.equalToSuperview().offset(20)
            make.height.equalTo(60)
            make.width.equalTo(KScreenWidth - 20*2)
        }
        //
        tagView.snp.makeConstraints { (make) in
          make.left.right.equalToSuperview().offset(20)
          make.bottom.equalTo(kolBtn!.snp.top).offset(-10)
          make.height.equalTo(64)
        }
        //
        tagTextLabel?.snp.makeConstraints({ (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo((64 - 18 - 15 - 5)/2)
            make.width.equalToSuperview()
            make.height.equalTo(18.0)
        })
        //
        tagTimeLabel?.snp.makeConstraints({ (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo((tagTextLabel?.snp.bottom)!).offset(5.0)
            make.height.equalTo(15.0)
        })
        //
        tagLabel?.snp.makeConstraints({ (make) in
            make.bottom.equalTo(tagView.snp.top).offset(-10)
            make.right.left.equalToSuperview()
            make.height.equalTo(35)
        })
        //
        lineLabel?.snp.makeConstraints({ (make) in
            make.bottom.equalTo((tagLabel?.snp.top)!).offset(-10)
            make.height.equalTo(0.5)
            make.left.right.equalToSuperview()
        })
        //
        titleLabel?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(16)
            make.top.equalTo((bgIv?.snp.bottom)!).offset(45)
            make.height.equalTo(18)
        })
        //
        subTitleLabel?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(titleLabel!)
            make.top.equalTo((titleLabel?.snp.bottom)!).offset(6)
            make.height.equalTo(13)
        })
        //
        timeLabel?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(titleLabel!)
            make.top.equalTo((subTitleLabel?.snp.bottom)!).offset(5)
            make.height.equalTo(15)
        })
        //
        contentTV.snp.makeConstraints { (make) in
            make.width.equalTo(KScreenWidth)
            make.top.equalTo((timeLabel?.snp.bottom)!).offset(5)
            make.bottom.equalTo((lineLabel?.snp.bottom)!).offset(-5)
        }
        kolLabel = UILabel.initFrom(frame: CGRect(x:0, y:0,width: KScreenWidth - 2 * 20,height:15), title:"已有\(String(describing: activityEntity!.invitees.count))人参加" , titleColor: UIColor.withHex(hexString: SysColorBlack), textAligment: .center, backColor:UIColor.clear, font: UIFont.font(s: 11))
        kolBtn?.addSubview(kolLabel!)
        
        //
        let temw = 40.0
        let temg = 18.0
        var num = (activityEntity?.invitees.count)! + 1
        if num > 4 {
            num = 5
        }
        let a = KScreenWidth - 2 * 20 - (CGFloat)((Int)(temw)*num)
        let b = (a - CGFloat((Int)(temg) * (num - 1)))/2
        for index in 0...num - 1 {
            if index == (num - 1){
                let kolLabel1 = UILabel.initFrom(frame: CGRect(x:b + (CGFloat)(temw+temg)*(CGFloat)(index), y: (kolLabel?.bottom)! + 5.0, width: CGFloat(temw), height: CGFloat(temw)), title: "更多", titleColor: UIColor.black, textAligment: .center, backColor: UIColor.clear, font: UIFont.font(s: 15))
                kolLabel1?.layer.cornerRadius = CGFloat(temw/2)
                kolLabel1?.layer.borderColor = UIColor.cyan.cgColor
                kolLabel1?.layer.borderWidth = 1.0
                kolLabel1?.layer.masksToBounds = true
                kolBtn?.addSubview(kolLabel1!)
            }else{
                let kolIV = UIImageView.init(frame: CGRect(x: Double(b) + (Double)(index) * (Double)(temw + temg), y: Double((kolLabel?.bottom)! + 5), width: temw, height: temw))
                let userEntity = activityEntity?.invitees[index]
                if (userEntity?.avatar_url!.isEmpty)!{
                    kolIV.layer.cornerRadius = CGFloat(temw/2)
                    kolIV.layer.borderWidth = 1.0
                    kolIV.layer.borderColor = UIColor.red.cgColor
                    kolIV.layer.masksToBounds = true
                }else{
                    let url = URL(string: (userEntity?.avatar_url!)!)
                    kolIV.kf.setImage(with: url)
                    kolIV.layer.cornerRadius = CGFloat(temw/2)
                    kolIV.layer.masksToBounds = true
                }
                kolBtn?.addSubview(kolIV)
            }
        }
        self.view.bringSubview(toFront:self.navView!)
        self.updateText()
    }
    
    @objc  func kolBtnAction() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
