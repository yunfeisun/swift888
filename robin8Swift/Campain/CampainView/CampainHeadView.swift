//
//  CampainHeadView.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/24.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit
import LLCycleScrollView
class CampainHeadView: UICollectionReusableView,UIScrollViewDelegate {
    var scroll:LLCycleScrollView?
    
    func setBanner(bannerList:NSArray) {
        scroll?.imagePaths = bannerList as! Array<String>
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        scroll = LLCycleScrollView.llCycleScrollViewWithFrame(self.bounds)
        scroll?.pageControlPosition = .center
        scroll?.pageControlTintColor = UIColor.white
        scroll?.pageControlCurrentPageColor = UIColor.yellow
        scroll?.lldidSelectItemAtIndex = {index in

        }
        self.addSubview(scroll!)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
