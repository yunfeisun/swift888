//
//  collectView.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/24.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
protocol collectViewDelegate {
    func selectItemAction()
}

class collectView: UIView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var page = 0
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return datalist.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CampainCell", for: indexPath) as! CampainCell
        let model = datalist[indexPath.row] as kolEntity
        let url = URL(string: model.avatar_url!)
        cell.headImageView.kf.setImage(with: url)
        cell.nameLabel.text = model.name!
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (KScreenWidth - 15)/2, height: (KScreenWidth - 15)/2)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: KScreenWidth, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: KScreenWidth, height: 0.1)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.selectItemAction()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let resuableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CampainHeadView", for: indexPath) as! CampainHeadView
            if self.bannerList.count > 0{
                resuableView.setBanner(bannerList: self.bannerList as NSArray)
            }
            return resuableView
        }else{
            let resuableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "CampainFootView", for: indexPath) as! CampainHeadView
            
            return resuableView
        }
        
    }
    var collect:UICollectionView?
    var delegate:collectViewDelegate?
    var bannerList:[String] = []
    var datalist:[kolEntity] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let layout = UICollectionViewFlowLayout.init()
        layout.itemSize = CGSize.init(width: 100, height: 100)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        collect = UICollectionView.init(frame: self.bounds, collectionViewLayout: layout)
        collect?.delegate = self
        collect?.dataSource = self
        collect?.register(UINib.init(nibName: "CampainCell", bundle: nil), forCellWithReuseIdentifier: "CampainCell")
        collect?.register(CampainHeadView.classForCoder(), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CampainHeadView")
        collect?.register(CampainHeadView.classForCoder(), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "CampainFootView")
        collect?.backgroundColor = UIColor.white
        self.addSubview(collect!)
        addRefresh()
    }
    func addRefresh() {
        collect?.es.addPullToRefresh {
            self.datalist.removeAll()
            self.page = 0
            self.requstData()
        }
        collect?.es.startPullToRefresh()
        collect?.es.addInfiniteScrolling {
            self.page += 1
            self.requstData()
        }
    }
    func requstData() {
        YFLoadingProvider.request1(.HomeUrl(page: self.page + 1, banner: "Y", order: "order_by_hot"), completion: { (result) in
            let arr = result["kol_announcements"].arrayValue
            self.bannerList.removeAll()
            for index in 0...arr.count-1{
                let jsonData = JSON(arr[index])
                let bannerModel = kolBannerModel.init(jsonData: jsonData)
                self.bannerList.append(bannerModel.cover_url!)
            }
            self.datalist += dealData.kolData(jsonObject: result) as! [kolEntity]
            self.collect?.es.stopPullToRefresh()
            self.collect?.es.stopLoadingMore()
            let kolArr:[kolEntity] = dealData.kolData(jsonObject: result) as! [kolEntity]
            if kolArr.count < 20{
                self.collect?.es.removeRefreshFooter()
            }
            self.collect?.reloadData()
        }) { (error) in
            print(error)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
