//
//  CampainCell.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/24.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit

class CampainCell: UICollectionViewCell {

    @IBOutlet weak var headImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var kolLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
