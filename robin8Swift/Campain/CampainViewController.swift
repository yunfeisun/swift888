//
//  CampainViewController.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/10.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit

class CampainViewController: BaseViewController, collectViewDelegate{
    func selectItemAction() {
        print("hehe")
    }
    
    var campainView:collectView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "活动"
        self.view.backgroundColor = UIColor.green
        campainView = collectView.init(frame: CGRect(x: 0, y: 64, width: KScreenWidth, height: KScreenHeight - 64 - 49))
        campainView?.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.addSubview(campainView!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
