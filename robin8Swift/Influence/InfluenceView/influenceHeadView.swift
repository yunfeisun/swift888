//
//  influenceHeadView.swift
//  robin8Swift
//
//  Created by RB8 on 2018/2/1.
//  Copyright © 2018年 robin8. All rights reserved.
//

import UIKit

class influenceHeadView: UIView {
    var scoreLabel:UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var  radius: CGFloat = 80{
        didSet{
            setNeedsDisplay()
        }
    }
    var lineWidth: CGFloat = 5.0{
        didSet{
            setNeedsDisplay()
        }
    }
    var lineColor = UIColor.gray{
        didSet{
            setNeedsDisplay()
        }
    }
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
      // Drawing code
        pathForCircle()
    }
    func pathForCircle(){
        lineColor.set()
        let path1 = UIBezierPath(arcCenter:CGPoint(x: self.bounds.size.width/2, y: 100) , radius: radius, startAngle: CGFloat.pi, endAngle: 0, clockwise: true)
        path1.lineWidth = lineWidth
        path1.stroke()
        let path = UIBezierPath(arcCenter:CGPoint(x: self.bounds.size.width/2, y: 100) , radius: radius, startAngle: CGFloat.pi, endAngle:-CGFloat.pi/2 , clockwise: true)
        
//        path.lineWidth = 5
        path.lineCapStyle = .round
//        path.stroke()
        let lineShape = CAShapeLayer()
        lineShape.frame = self.bounds
        lineShape.path = path.cgPath
        lineShape.lineWidth = 5
        lineShape.strokeColor = UIColor.red.cgColor
        lineShape.fillColor = UIColor.clear.cgColor
        self.layer.addSublayer(lineShape)
        let pathAniamtion = CABasicAnimation.init(keyPath: "strokeEnd")
        pathAniamtion.duration = 1
        pathAniamtion.timingFunction = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionEaseOut)
        pathAniamtion.fromValue = 0
        pathAniamtion.toValue = 1
        pathAniamtion.autoreverses = false
        //保持在最后一帧
        pathAniamtion.fillMode = kCAFillModeForwards
        //动画结束后不移除
        pathAniamtion.isRemovedOnCompletion = false
        lineShape.add(pathAniamtion, forKey: "strokeEndAnimation")
        //
        scoreLabel = UILabel.init(frame: CGRect(x: 0, y: 70, width: self.bounds.size.width, height: 20))
        scoreLabel?.textColor = UIColor.black
        scoreLabel?.font = UIFont.systemFont(ofSize: 15)
        scoreLabel?.textAlignment = .center
        scoreLabel?.text = "40分"
        self.addSubview(scoreLabel!)
    }

}
