//
//  influenceMiddleView.swift
//  robin8Swift
//
//  Created by RB8 on 2018/2/9.
//  Copyright © 2018年 robin8. All rights reserved.
//

import UIKit

class influenceMiddleView: UIView {
//    let backScroll:UIScrollView?
//    let weiboView:BigWeiboView?
//    let wechatView:BigWechatView?
    var topView:UIView?
    var weiboBtn:UIButton?
    var wechatBtn:UIButton?
    var lineLabel:UILabel?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        topView = UIView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height:50))
        topView?.backgroundColor = UIColor.white
        self.addSubview(topView!)
        //
        weiboBtn = UIButton.initfrom(frame: CGRect(x: 0, y: 0, width: KScreenWidth/2, height:50), title: "微博", hltitle: nil, backColor: UIColor.white, titleColor: UIColor.black, titleFont: UIFont.systemFont(ofSize: 18), image: nil)
        weiboBtn?.tag = 100
        topView?.addSubview(weiboBtn!)
        weiboBtn?.addTarget(self, action: #selector(changeFrame), for: .touchUpInside)
        //
        wechatBtn = UIButton.initfrom(frame: CGRect(x: KScreenWidth/2, y: 0, width: KScreenWidth/2, height:50), title: "微信", hltitle: nil, backColor:UIColor.white, titleColor: UIColor.black, titleFont: UIFont.systemFont(ofSize: 18), image: nil)
        wechatBtn?.tag = 101
        topView?.addSubview(wechatBtn!)
        wechatBtn?.addTarget(self, action: #selector(changeFrame), for: .touchUpInside)
        //
        weiboBtn?.sizeToFit()
        weiboBtn?.frame = CGRect(x: 0, y: 0, width: KScreenWidth/2, height:50)
        lineLabel = UILabel.initFrom(frame:CGRect(x:(KScreenWidth/2 - 40)/2, y: (topView?.bottom)! - 1, width: 40, height:1) , title: nil, titleColor: UIColor.blue, textAligment:.center, backColor: UIColor.blue, font: nil)
        topView?.addSubview(lineLabel!)
        //
    }
    @objc func changeFrame(btn:UIButton) {
        if btn.tag == 100{
            UIView.animate(withDuration: 1.0, animations: {
                self.lineLabel?.frame = CGRect(x:(KScreenWidth/2 - 40)/2, y: (self.topView?.bottom)! - 1, width: 40, height:1)
            })
           
        }else{
            UIView.animate(withDuration: 1.0, animations: {
                self.lineLabel?.frame = CGRect(x:(KScreenWidth/2 - 40)/2 + KScreenWidth/2, y: (self.topView?.bottom)! - 1, width: 40, height:1)
            })
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
    }
    

}
