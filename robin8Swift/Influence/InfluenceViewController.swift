//
//  InfluenceViewController.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/10.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit

class InfluenceViewController: UIViewController,UIScrollViewDelegate {
    var scroll:UIScrollView?
    var topView:influenceHeadView?
    var smallScroll:UIScrollView?
    var middleView:influenceMiddleView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.title = "影响力"
        self.automaticallyAdjustsScrollViewInsets = false
        // Do any additional setup after loading the view.
        scroll = UIScrollView.init(frame: CGRect(x: 0, y: navHeight, width: KScreenWidth, height: KScreenHeight - navHeight))
        scroll?.showsVerticalScrollIndicator = false
        scroll?.showsHorizontalScrollIndicator = false
        scroll?.delegate = self
        scroll?.bounces = false
        self.view.addSubview(scroll!)
        
        topView = influenceHeadView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: 100))
        topView?.backgroundColor = UIColor.clear
        scroll!.addSubview(topView!)
        initUI()
    }
    func initUI() {
        let upBtn = UIButton.init(frame: CGRect(x: (KScreenWidth - 160)/2, y: (topView?.bottom)! + 15, width: 160, height: 30))
        upBtn.addTarget(self, action: #selector(upInfluence), for:.touchUpInside)
        upBtn.layer.cornerRadius = 30/2
        upBtn.layer.masksToBounds = true
        upBtn.setTitleColor(.white, for: .normal)
        upBtn.backgroundColor = UIColor.withHex(hexString: SysColorYellow)
        upBtn.setTitle("提升影响力", for: .normal)
        scroll!.addSubview(upBtn)
        //
        let label = UILabel.init(frame: CGRect(x: 15, y: upBtn.bottom + 30, width: KScreenWidth - 30, height: 15))
        label.textAlignment = .left
        label.textColor = UIColor.black
        label.text = "根据您的兴趣，推荐以下用户"
        label.backgroundColor = UIColor.clear
        label.font = UIFont.systemFont(ofSize: 14)
        scroll?.addSubview(label)
        //
        smallScroll = UIScrollView.init(frame: CGRect(x: 15, y: label.bottom + 10, width: KScreenWidth - 30, height: 30))
        smallScroll?.delegate = self
        smallScroll?.showsHorizontalScrollIndicator = false
        smallScroll?.showsVerticalScrollIndicator = false
        smallScroll?.bounces = false
        scroll?.addSubview(smallScroll!)
        for index in 0...5 {
            let view = UIView.init(frame: CGRect(x: (30 + 10) * index, y: 0, width: 30, height: 30))
            view.layer.cornerRadius = 30/2
            view.layer.borderColor = UIColor.gray.cgColor
            view.layer.borderWidth = 1.0
            view.layer.masksToBounds = true
            smallScroll?.addSubview(view)
            //
            let btn = UIButton.init(frame: CGRect(x: 2, y: 2, width: 26, height: 26))
            btn.layer.cornerRadius = 26/2
            btn.layer.masksToBounds = true
            btn.addTarget(self, action: #selector(otherInfluence(btn:)), for: .touchUpInside)
            if index == 0{
                btn.setImage(UIImage.init(named: "AddFriend"), for: .normal)
            }else{
                btn.setImage(UIImage.init(named: "set"), for: .normal)
            }
            view.addSubview(btn)
        }
        //lineView
        let lineView = UIView.init(frame: CGRect(x: 0, y: smallScroll!.bottom + 20, width: KScreenWidth, height: 8))
        lineView.backgroundColor = UIColor.withHex(hexString: "f5f5f5")
        scroll?.addSubview(lineView)
        //
        middleView = influenceMiddleView.init(frame:CGRect(x: 0, y: lineView.bottom, width: KScreenWidth, height: 50))
        scroll?.addSubview(middleView!)
    }
    @objc func otherInfluence(btn:UIButton) {
        
    }
    @objc func upInfluence(){
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
