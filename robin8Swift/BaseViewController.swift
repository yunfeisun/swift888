//
//  BaseViewController.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/27.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit
 @objc protocol  BaseVCDelegate{
    @objc optional  func NavLeftBtnAction()
    @objc optional  func NavRightBtnAction()
}
class BaseViewController: UIViewController,UIGestureRecognizerDelegate {
    
    var bgIv:UIImageView?
    var navView:UIView?
    var navTitleLabel:UILabel?
    var navLeftBtn:UIButton?
    var navLeftImg:UIImageView?
    var edgePGR:UIScreenEdgePanGestureRecognizer?
    var delegate:BaseVCDelegate?
    var navRightBtn:UIButton?
    var navRightImg:UIImageView?
    
    
    var navRightTitle:String = ""{
        willSet{
            if newValue == "collect" {
                navRightImg?.image = UIImage.init(named: "collect")
            }else if newValue == "set"{
                navRightImg?.image = UIImage.init(named: "set")
            }else if newValue == "完成"{
                navRightBtn?.setTitle("完成", for: .normal)
            }
        }
    }
    
    var navLeftTitle:String? = nil{
        willSet{
            if newValue == nil{
                navLeftImg?.removeFromSuperview()
            }
        }
    }
    
    var navTitle:String = ""{
        willSet{
            if  newValue.count > 0 {
                loadNavView()
                self.navTitleLabel?.text = newValue
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.bgIv = UIImageView.init(frame: self.view.bounds)
        self.bgIv?.image = UIImage.init(named: "pic_bg.jpg")
        self.view.addSubview(self.bgIv!)
        self.bgIv?.isHidden = true
        // Do any additional setup after loading the view.
    }
    func loadNavView() {
        if navView != nil {
            return
        }
        self.navView = UIView.init(frame: CGRect(x: 0, y: 0, width: KScreenWidth, height: navHeight))
        self.navView?.backgroundColor = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5)
        self.navView?.isUserInteractionEnabled = true
        self.view.addSubview(self.navView!)
        //
        self.navTitleLabel = UILabel.init(frame: CGRect(x: 50, y:StatusHeight , width: KScreenWidth - 100, height: 44))
        self.navTitleLabel?.font = UIFont.monospacedDigitSystemFont(ofSize: 17, weight: .medium)
        self.navTitleLabel?.textAlignment = .center
        self.navView?.addSubview(self.navTitleLabel!)
        //
        self.navLeftBtn = UIButton.init(type: .custom)
        self.navLeftBtn?.frame = CGRect(x: 0, y: StatusHeight, width: 44.0, height: 44.0)
        self.navLeftBtn?.addTarget(self, action: #selector(navLeftBtnAction), for: .touchUpInside)
        self.navLeftBtn?.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        self.navLeftBtn?.setTitleColor(UIColor.black, for: .normal)
        self.navView?.addSubview(self.navLeftBtn!)
        //
        self.navLeftImg = UIImageView.init(frame:CGRect(x: 12.0, y: (44 - 20)/2, width: 20, height: 20))
        self.navLeftImg?.image = UIImage.init(named: "back")
        self.navLeftBtn?.addSubview(self.navLeftImg!)
        //
        self.navRightBtn = UIButton.init(type: .custom)
        navRightBtn?.frame = CGRect(x: KScreenWidth - 44.0, y: StatusHeight, width: 44, height: 44)
        navRightBtn?.tag = 10000
        navRightBtn?.addTarget(self, action: #selector(navRightBtnAction), for: .touchUpInside)
        navRightBtn?.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        navRightBtn?.setTitleColor(UIColor.black, for:.normal)
        self.navView?.addSubview(navRightBtn!)
        //
        navRightImg = UIImageView.init(frame: CGRect(x: (navRightBtn?.frame.size.width)! - 20.0 - 12, y: (44 - 20)/2, width: 20, height: 20))
        navRightImg?.isUserInteractionEnabled = true
        self.navRightBtn?.addSubview(navRightImg!)
        //
        self.edgePGR = UIScreenEdgePanGestureRecognizer.init(target: self, action: #selector(edgePanGesture(sender:)))
        self.edgePGR?.delegate = self
        self.edgePGR?.edges = .left
        self.view.addGestureRecognizer(self.edgePGR!)
    }
    @objc func navRightBtnAction() {
        delegate?.NavRightBtnAction!()
    }
    @objc func navLeftBtnAction(){
        delegate?.NavLeftBtnAction!()
    }
    @objc func edgePanGesture(sender:UIScreenEdgePanGestureRecognizer) {
        navLeftBtnAction()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
