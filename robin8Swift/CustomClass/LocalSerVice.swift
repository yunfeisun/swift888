//
//  LocalSerVice.swift
//  robin8Swift
//
//  Created by RB8 on 2017/12/27.
//  Copyright © 2017年 robin8. All rights reserved.
//

import Foundation
class LocalService {
    //保存用户UserLoginId
    static func setRBLocalDataUserLoginIdWith(str:String?) {
     let defaults = UserDefaults.standard
     defaults.set(str, forKey: "UserLoginId")
     defaults.synchronize()
    }
    //读取用户UserLoginId
    static func getRBLocalDataUserLoginId() -> String?{
        let defaults = UserDefaults.standard
        if defaults.object(forKey: "UserLoginId") == nil{
            return nil
        }
        return defaults.object(forKey: "UserLoginId") as? String
    }
    //保存用户UserLoginName
    static func setRBLocalDataUserLoginNameWith(str:String?){
        let ud = UserDefaults.standard
        ud.set(str, forKey: "UserLoginName")
        ud.synchronize()
    }
    //读取用户UserLoginName
    static func getRBLocalDataUserLoginName() -> String?{
        let ud = UserDefaults.standard
        if ud.object(forKey: "UserLoginName") == nil{
            return nil
        }
        return ud.object(forKey: "UserLoginName") as? String
    }
    static func setRBLocalDataUserLoginPhoneWith(str:String?){
        let ud = UserDefaults.standard
        ud.set(str, forKey: "UserLoginPhone")
        ud.synchronize()
    }
    static func getRBLocalDataUserLoginPhone() -> String?{
        let ud = UserDefaults.standard
        if ud.object(forKey: "UserLoginPhone") == nil{
            return nil
        }
        return ud.object(forKey: "UserLoginPhone") as? String
    }
    static func setRBLocalDataUserLoginAvatarWith(str:String?){
        let  ud = UserDefaults.standard
        ud.set(str, forKey: "UserLoginAvatar")
        ud.synchronize()
    }
    static func getRBLocalDataUserLoginAvatar() -> String?{
        let ud = UserDefaults.standard
        if ud.object(forKey: "UserLoginAvatar") == nil {
            return nil
        }
        return ud.object(forKey: "UserLoginAvatar") as? String
    }
    static func setRBLocalDataUserPrivateTokenWith(str:String?){
        let ud = UserDefaults.standard
        if str == nil{
            ud.set(nil, forKey: "PrivateToken")
        }else{
            let dic = JWT.decodeMessage(str, withSecret: "Robin888") as Dictionary
            let jwtDic = dic["payload"] as? [String:AnyObject]
            if let token = jwtDic!["private_token"]{
                ud.set(token, forKey: "PrivateToken")
            }
        }
        ud.synchronize()
    }
    static func getRBLocalDataUserPrivateToken() -> String?{
        let ud = UserDefaults.standard
        if ud.object(forKey: "PrivateToken") == nil{
            return nil
        }
        return ud.object(forKey: "PrivateToken") as? String
    }
    static func setRBLocalDataUserLoginKolIdWith(str:String?){
        let ud = UserDefaults.standard
        ud.set(str, forKey: "UserLoginKolId")
        ud.synchronize()
    }
    static func getRBLocalDataUserLoginKolId() -> String?{
        let ud = UserDefaults.standard
        if ud.object(forKey: "UserLoginKolId") == nil{
            return nil
        }
        return ud.object(forKey: "UserLoginKolId") as? String
    }
}
