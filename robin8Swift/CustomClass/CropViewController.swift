//
//  CropViewController.swift
//  robin8Swift
//
//  Created by RB8 on 2018/1/24.
//  Copyright © 2018年 robin8. All rights reserved.
//

import UIKit
import Alamofire
class CropViewController: BaseViewController,BaseVCDelegate {
    var imageView:UIImageView?
    var coperImage = UIImage.init(named: "aaa.jpeg")
    var pointValue:CGPoint = CGPoint(x: 0, y: 0)
    var coperImageView:UIImageView?
    var imageHeight:CGFloat?
    var headImage = UIImage.init()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageHeight = (coperImage?.size.height)! * KScreenWidth/(coperImage?.size.width)!
        
        imageView = UIImageView.init(frame: CGRect(x: 0, y: navHeight + (KScreenHeight-navHeight - imageHeight!)/2, width: KScreenWidth, height: imageHeight!))
        imageView?.image = coperImage
        self.view.addSubview(imageView!)
        //
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizer(sender:)))
        imageView?.isUserInteractionEnabled = true
        imageView?.addGestureRecognizer(panGestureRecognizer)
        //
        coperImageView = UIImageView.init(frame:CGRect(x: (KScreenWidth - 200)/2, y:navHeight + (KScreenHeight - navHeight - 200)/2 , width: 200, height: 200))
        self.view.addSubview(coperImageView!)
        //
        let linePath = UIBezierPath.init(rect: CGRect(x: (KScreenWidth - 200)/2, y: (KScreenHeight - navHeight - 200)/2, width: 200, height: 200))
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = CGRect.init(x: 0, y: navHeight, width: KScreenWidth, height: KScreenHeight - navHeight)
        shapeLayer.backgroundColor = UIColor.init(red: 50/255.0, green: 50/255.0, blue: 50/255.0, alpha: 0.3).cgColor
        shapeLayer.path = linePath.cgPath
        shapeLayer.lineWidth = 1
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        self.view.layer.addSublayer(shapeLayer)
        self.navTitle = "图片裁剪"
        self.delegate = self
        self.navRightTitle = "完成"
    }

    @objc func panGestureRecognizer(sender:UIPanGestureRecognizer) {
        let _tranX = sender.translation(in: imageView).x
        let _tranY = sender.translation(in: imageView).y
        imageView?.transform = CGAffineTransform(translationX: _tranX + pointValue.x, y: _tranY + pointValue.y)
        if sender.state == .ended{
            pointValue.x += _tranX
            pointValue.y += _tranY
            if pointValue.x > (KScreenWidth - 200)/2{
                pointValue.x = (KScreenWidth - 200)/2
            }
            if pointValue.x  < -(KScreenWidth - 200)/2{
                pointValue.x = -(KScreenWidth - 200)/2
            }
            if pointValue.y < (KScreenHeight - navHeight - imageHeight!)/2 - (KScreenHeight - navHeight - 200)/2  {
                pointValue.y = (KScreenHeight - navHeight - imageHeight!)/2 - (KScreenHeight - navHeight - 200)/2
            }
            if pointValue.y > (KScreenHeight - navHeight - 200)/2 - (KScreenHeight-navHeight - imageHeight!)/2{
                pointValue.y = (KScreenHeight - navHeight - 200)/2 - (KScreenHeight-navHeight - imageHeight!)/2
            }
            imageView?.transform = CGAffineTransform(translationX:pointValue.x, y:pointValue.y)
            
            print(imageView?.frame ?? 0)
        }
    }
    override func navLeftBtnAction() {
        self.dismiss(animated: true, completion: nil)
    }
    //裁剪并上传
    override func navRightBtnAction() {
        let scale = (imageView?.image?.size.width)!/(KScreenWidth*2)
        let rect = CGRect(x:((KScreenWidth - 200)/2 - (imageView?.frame.origin.x)!) * scale * 2, y: ((imageHeight! - 200)/2 - pointValue.y) * scale * 2, width:200 * scale * 2, height: 200 * scale * 2)
        coperImageView?.image = imageView?.image?.crop(bounds:rect)
        headImage = (coperImageView?.image)!
        imageView?.removeFromSuperview()
        
        let headers = ["Authorization" : JWTSecret.getSecret()]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImageJPEGRepresentation((self.coperImageView?.image)!, 0.8)!, withName: "avatar", fileName: "avatar.jpg", mimeType: "image/jpeg")
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: APIURL + "api/v1/kols/upload_avatar", method: .post, headers: headers as? HTTPHeaders) { (result) in
            print("数据准备完成")
            switch result{
            case .success(let upload,_,_):
                upload.responseJSON(completionHandler: { (response) in
                    print(response.result.value as! NSDictionary)
                    self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                })
            case .failure(let encodingError):
                //                completion(nil,encodingError)
                print(encodingError)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
