//
//  JWTSecret.m
//  robin8Swift
//
//  Created by RB8 on 2017/12/7.
//  Copyright © 2017年 robin8. All rights reserved.
//

#import "JWTSecret.h"
#import "JWT.h"
@implementation JWTSecret

+(NSString*)getSecret{
    NSDate*nowDate = [NSDate date];
    NSDate*utcDate = [self getNowDateFromatAnDate:nowDate];
    NSString*dateStr = [NSString stringWithFormat:@"%f",[utcDate timeIntervalSince1970]];
    NSString * tokenStr= [[NSUserDefaults standardUserDefaults]objectForKey:@"PrivateToken"];
    if(tokenStr==nil) {
        tokenStr = @"";
    }
    JWTAlgorithmHS256*algorithm = [[JWTAlgorithmHS256 alloc]init];
    NSString * authorization = [JWT encodePayload:@{@"get_code":@"get_code",@"is_new":@"is_new",@"private_token":tokenStr,@"time":dateStr} withSecret:@"Robin888" algorithm:algorithm];
    return authorization;
}
+ (NSDate *)getNowDateFromatAnDate:(NSDate *)anyDate{
    //设置源日期时区
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    //设置转换后的目标日期时区
    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
    //得到源日期与世界标准时间的偏移量
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:anyDate];
    //目标日期与本地时区的偏移量
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:anyDate];
    //得到时间偏移量的差值
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    //转为现在时间
    NSDate* destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:anyDate];
    return destinationDateNow;
}
+(NSString*)getPrivateToken:(NSString*)private_token{
    NSDictionary*jwtStr=[JWT decodeMessage:private_token withSecret:@"Robin888"];
    NSArray*jwtArray = [jwtStr valueForKey:@"payload"];
    NSString*token = [NSString stringWithFormat:@"%@",[jwtArray valueForKey:@"private_token"]];
    return token;
}

@end
