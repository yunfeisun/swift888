//
//  JWTSecret.h
//  robin8Swift
//
//  Created by RB8 on 2017/12/7.
//  Copyright © 2017年 robin8. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JWTSecret : NSObject
+(NSString*)getSecret;
+(NSString*)getPrivateToken:(NSString*)private_token;
@end
