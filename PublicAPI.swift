//
//  PublicAPI.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/15.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit
import Kingfisher
let KScreenWidth = UIScreen.main.bounds.width
let KScreenHeight = UIScreen.main.bounds.height
let navHeight = UIApplication.shared.statusBarFrame.size.height + 44
let StatusHeight = UIApplication.shared.statusBarFrame.size.height


//develop222
let version_number = 4

//develop
//let api = "develop.api.net"

//master
let api = "master.api.net"

//staging
//let api = "staging.api.net"

//let APIURL = "http://api.robin8.net/"
let APIURL = "https://qa.robin8.net/"
var topVC:UIViewController?{
    var resultVC: UIViewController?
    resultVC = _topVC(UIApplication.shared.keyWindow?.rootViewController)
    while resultVC?.presentedViewController != nil {
        resultVC = _topVC(resultVC?.presentedViewController)
    }
    
    
    
    
    return resultVC
}
private func _topVC(_ vc: UIViewController?) -> UIViewController?{
    if vc is UINavigationController{
        return _topVC((vc as? UINavigationController)?.topViewController)
    }else if vc is UITabBarController{
        return _topVC((vc as? UITabBarController)?.selectedViewController)
    }else{
        return vc
    }
}
class PublicAPI: NSObject {
    //设备系统号
    class  func getCurrentDeviceSystemVersion() -> String {
        return UIDevice.current.systemVersion
    }
    //应用版本
    class  func getCurrentBundleShortVersion() -> String {
        return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    }
    class func getUIDateCompareNow(dateStr:String) -> String{
        if dateStr.components(separatedBy: "T").count == 1 {
            return ""
        }
        let inputDateStr = dateStr.components(separatedBy: "T")[0]
        var inputTimeStr = dateStr.components(separatedBy: "T")[1]
        inputTimeStr = inputTimeStr.components(separatedBy: "+")[0]
        let formatter = DateFormatter.init()
        if inputDateStr.components(separatedBy: "-").count > 1 {
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        }else{
            formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        }
        let inputDate = formatter.date(from: inputDateStr + " " + inputTimeStr)
        //  formatter.date(from: inputDateStr + inputTimeStr)
        let fromZone = TimeZone.current
        let fromInterval = fromZone.secondsFromGMT(for: inputDate!)
        let fromDate = inputDate?.addingTimeInterval(Double(fromInterval))
        
        let adate = Date()
        let zone = TimeZone.current
        let interval = zone.secondsFromGMT(for: adate)
        let localeDate = adate.addingTimeInterval(Double(interval))
        
        let  interValTime = (fromDate?.timeIntervalSinceReferenceDate)! - localeDate.timeIntervalSinceReferenceDate
        let ITime = fabs(interValTime)
        let iSeconds = Int(ITime.truncatingRemainder(dividingBy:60))
        let iMinutes = Int((ITime/60)/60)
        let iHours = Int(fabs((ITime/3600).truncatingRemainder(dividingBy: 24)))
        let iDays:Int = Int(ITime/60/60/24)
        var timeString = ""
        if ITime < 0 {
            timeString = "活动已结束"
        }else{
            if iHours < 1 && iMinutes > 0 {
                timeString = "还有\(iMinutes)分钟"
            }else if iHours > 0 && iDays < 1 && iMinutes > 0{
                timeString = "还有\(iHours)小时"
            }else if iHours > 0 && iDays < 1{
                timeString = "还有\(iHours)小时"
            }else if iDays > 0 && iHours > 0{
                timeString = "还有\(iDays)天\(iHours)小时"
            }else if iDays > 0 {
                timeString = "还有\(iDays)天"
            }else{
                timeString = "还有\(iSeconds)秒"
            }
        }
        return timeString
    }
//    class func getUIDateFromatAnyDate(anyDate:NSDate) -> NSDate {
//
//    }
}
extension UIColor {
    /**
     获取颜色，通过16进制色值字符串，e.g. #ff0000， ff0000
     - parameter hexString  : 16进制字符串
     - parameter alpha      : 透明度，默认为1，不透明
     - returns: RGB
     */
    static func withHex(hexString hex: String, alpha:CGFloat = 1) -> UIColor {
        // 去除空格等
        var cString: String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        // 去除#
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        // 必须为6位
        if (cString.count != 6) {
            return UIColor.gray
        }
        // 红色的色值
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        // 字符串转换
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: alpha)
    }
    
    /**
     获取颜色，通过16进制数值
     - parameter hexInt : 16进制数值
     - parameter alpha  : 透明度
     - returns : 颜色
     */
    static func withHex(hexInt hex:Int32, alpha:CGFloat = 1) -> UIColor {
        let r = CGFloat((hex & 0xff0000) >> 16) / 255
        let g = CGFloat((hex & 0xff00) >> 8) / 255
        let b = CGFloat(hex & 0xff) / 255
        return UIColor(red: r, green: g, blue: b, alpha: alpha)
    }
    
    /**
     获取颜色，通过rgb
     - parameter red    : 红色
     - parameter green  : 绿色
     - parameter blue   : 蓝色
     - returns : 颜色
     */
    static func withRGB(_ red:CGFloat, _ green:CGFloat, _ blue:CGFloat) -> UIColor {
        return UIColor.withRGBA(red, green, blue, 1)
    }
    
    /**
     获取颜色，通过rgb
     - parameter red    : 红色
     - parameter green  : 绿色
     - parameter blue   : 蓝色
     - parameter alpha  : 透明度
     - returns : 颜色
     */
    static func withRGBA(_ red:CGFloat, _ green:CGFloat, _ blue:CGFloat, _ alpha:CGFloat) -> UIColor {
        return UIColor(red: red / 255, green: green / 255, blue: blue / 255, alpha: alpha)
    }
}
extension UIButton{
    static func initfrom(frame:CGRect?,title:String?,hltitle:String?,backColor:UIColor?,titleColor:UIColor?,titleFont:UIFont?,image:UIImage?) -> UIButton? {
        let btn = UIButton.init(frame: frame!)
        btn.setTitle(title, for: .normal)
        btn.setTitle(hltitle, for:.highlighted)
        btn.setTitleColor(titleColor, for: .normal)
        btn.backgroundColor = backColor
        btn.titleLabel?.font = titleFont
        btn.setImage(image, for: .normal)
        return btn
    }
}
extension UILabel{
    static   func initFrom(frame:CGRect?,title:String?,titleColor:UIColor?,textAligment:NSTextAlignment,backColor:UIColor?,font:UIFont?) -> UILabel? {
        let label = UILabel.init(frame: frame!)
        label.text = title
        label.textColor = titleColor
        label.textAlignment = textAligment
        label.backgroundColor = backColor
        label.font = font
        return label
    }
}
extension UIView{
    public var left:CGFloat{
        get{
            return self.frame.origin.x
        }
        set{
            var r = self.frame
            r.origin.x = newValue
            self.frame = r
        }
    }
    public var top:CGFloat{
        get{
            return self.frame.origin.y
        }
        set{
            var r = self.frame
            r.origin.y = newValue
            self.frame = r
        }
    }
    public var right:CGFloat{
        get{
            return self.frame.origin.x + self.frame.size.width
        }
        set{
            var r = self.frame
            r.origin.x = newValue - r.size.width
            self.frame = r
        }
    }
    public var bottom:CGFloat{
        get{
          return self.frame.origin.y + self.frame.size.height
        }
        set{
            var r = self.frame
            r.origin.y = newValue - r.size.height
            self.frame = r
        }
    }
    public var width:CGFloat{
        get{
            return self.frame.size.width
        }
        set{
            var r = self.frame
            r.size.width = newValue
            self.frame = r
        }
    }
    public var height:CGFloat{
        get{
            return self.frame.size.height
        }
        set{
            var  r = self.frame
            r.size.height = newValue
            self.frame = r
            
        }
    }
    public var centerX:CGFloat{
        get{
            return self.center.x
        }
        set{
            self.center = CGPoint(x: newValue, y: self.center.y)
        }
    }
    public var centerY:CGFloat{
        get{
            return self.center.y
        }
        set{
            self.center = CGPoint(x: self.center.x, y: newValue)
        }
    }
   
}
extension UIFont{
    class func font(s:CGFloat) -> UIFont?{
        let font = UIFont.systemFont(ofSize: s, weight:.light)
        return font
    }
    class func font_cu(s:CGFloat) -> UIFont?{
        let font = UIFont.systemFont(ofSize: s, weight: .medium)
        return font
    }
    class func font_cu_cu(s:CGFloat) -> UIFont?{
        let font = UIFont.systemFont(ofSize: s, weight:.heavy)
        return font
    }
}
let SysColorYellow = "f1bc00"
let SysColorBlue = "36cdd3"
let SysColorbackgray = "ececec"
let SysColorBlack = "181616"
let SysColorSubBlack = "424141"
let SysColorSubGray = "929292"


let SysColorRed = "f26d45"
let SysColorOrange = "2bdbe2"
let SysColorDeep = UIColor.withHex(hexString: "000000", alpha: 0.6)
let SysColorCover = UIColor.withHex(hexString: "000000", alpha: 0.3)









