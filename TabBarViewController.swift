//
//  TabBarViewController.swift
//  robin8Swift
//
//  Created by RB8 on 2017/11/13.
//  Copyright © 2017年 robin8. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    var _backView:UIView? = nil
    var items:NSArray = []
    let NameArr = ["首页","活动","影响力","我的"]
    let picArr = ["tabbar_0_l","tabbar_1_l","tabbar_2_l","tabbar_3_l"]
    let picSelectArr = ["tabbar_0_h","tabbar_1_h","tabbar_2_h","tabbar_3_h"]
    let VCArr = [KOLViewController(),CampainViewController(),InfluenceViewController(),MyViewController()]
    var NavVCArr:[NSObject] = [NSObject]()
    var nav:UINavigationController = UINavigationController()
    
    func transferStringToColor(_ colorStr:String) -> UIColor {
        
        var color = UIColor.red
        var cStr : String = colorStr.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if cStr.hasPrefix("#") {
            let index = cStr.index(after: cStr.startIndex)
            cStr = cStr.substring(from: index)
        }
        if cStr.characters.count != 6 {
            return UIColor.black
        }
        
        let rRange = cStr.startIndex ..< cStr.index(cStr.startIndex, offsetBy: 2)
        let rStr = cStr.substring(with: rRange)
        
        let gRange = cStr.index(cStr.startIndex, offsetBy: 2) ..< cStr.index(cStr.startIndex, offsetBy: 4)
        let gStr = cStr.substring(with: gRange)
        
        let bIndex = cStr.index(cStr.endIndex, offsetBy: -2)
        let bStr = cStr.substring(from: bIndex)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rStr).scanHexInt32(&r)
        Scanner(string: gStr).scanHexInt32(&g)
        Scanner(string: bStr).scanHexInt32(&b)
        
        color = UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
        
        return color
    }

    func CreatTabBar() {
        _backView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 49))
        for index in 0...3 {
            let KolNav = UINavigationController(rootViewController: VCArr[index])
            KolNav.tabBarItem = UITabBarItem.init(title: NameArr[index], image: UIImage.init(named: picArr[index])?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage.init(named: picSelectArr[index])?.withRenderingMode(UIImageRenderingMode.alwaysOriginal))
            KolNav.navigationBar.barTintColor = UIColor.white
            KolNav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black,NSAttributedStringKey.font:UIFont.systemFont(ofSize: 20)]
            NavVCArr.append(KolNav)
        }
        self.viewControllers = NavVCArr as? [UIViewController]
        UITabBar.appearance().barTintColor = UIColor.white
        UITabBarItem.appearance().setTitleTextAttributes(NSDictionary(object:UIColor.black, forKey:NSAttributedStringKey.foregroundColor as NSCopying) as? [NSAttributedStringKey : AnyObject], for:UIControlState.normal);
        UITabBarItem.appearance().setTitleTextAttributes(NSDictionary(object:transferStringToColor("ecb200"), forKey:NSAttributedStringKey.foregroundColor as NSCopying) as? [NSAttributedStringKey : AnyObject], for:UIControlState.selected)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CreatTabBar()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
