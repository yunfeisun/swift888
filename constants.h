//
//  constants.h
//  robin8Swift
//
//  Created by RB8 on 2017/11/10.
//  Copyright © 2017年 robin8. All rights reserved.
//

#ifndef constants_h
#define constants_h
#define ScreenWidth                          UIScreen.main.bounds.size.width

#define ScreenHeight                         UIScreen.main.bounds.size.height

#endif /* constants_h */
